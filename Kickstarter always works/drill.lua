function Drill:on_melee_hit(peer_id)
	if self._disable_upgrades or self._jammed == false then
		return
	end
	self._unit:timer_gui():set_jammed(false)
	self._unit:interaction():set_active(false, true)
	self._unit:interaction():check_for_upgrade()
end