--[[ * Skip Level by gir489 v4
	 *
	 * Changelog:
	 *      v1: Initial release
	 *      v2: Fixed some things, man. And some stuff. Wouldn't reccomend it.
	 *      v3: Added grabSmallLoot boolean.
	 *          Fixed mistake in comments for the legit variable.
	 *          Increased the meth bags on Cook Off to 125 for maximum boostage.
	 *    v3.1: Fixed a scenario where legit would be false but getallsecureables would grab items it's not supposed to.
	 *      v4: Refactored entire project.
	 *
	 *	Not for use in Pirate Perfagtion Trainer]]

--[[Set this to false if you want the script to secure more loot than normal.]]
local legit = false
--[[Set this to false if you don't want to grab small loot.]]
local grabSmallLoot = true
--[[Set this to false to not grab the gage packages, so your name doesn't display on the other's HUD.]]
local grabGageWhenClient = false

--[[* Function to secure loot.
    *
    * Programmer's Note: The true parameter lets it know not to broadcast it to clients if the user is host.]]
function secure( name )
	managers.loot:secure( name, managers.money:get_bag_value( name ), true )
end

--[[* Secures all the loot in the map.
    *
    * TODO: Make this code be more generic. Should be able to run on any map without map specific code.
    * Programmer's Note: Lua sucks. Needs more overloaded methods.]]
function getallsecureables( )
	local unit_list = World:find_units_quick( "all" )
	for _,unit in ipairs( unit_list ) do
		--[[Check for securable items.]]
		if ( unit:carry_data() ) then --[[Check if it's a carryable item.]]
			if ( ((unit:interaction():active() == true) and legit) or (legit == false) ) then
				secure(unit:carry_data():carry_id())
			end
		end
		if ( unit:interaction() ) then
			if ( unit:interaction().tweak_data == 'weapon_case' or unit:interaction().tweak_data == 'take_weapons' ) then
				secure("weapon") --[[I have to nest the secure("weapon") call in here, because it's just the weapon case.]]
			end
		end
		--[[Check for grabbable items]]
		if ( unit:base() ) then --[[Some units don't have a base.]]
			if ( unit:base().small_loot and grabSmallLoot ) then --[[Check if it's a grabbable.]]
				unit:interaction():interact(managers.player:player_unit())
			end
		end
		if ( grabGageWhenClient ) then
			if ( tostring(unit:name()) == "Idstring(@IDe8088e3bdae0ab9e@)" --[[Yellow Package]] or
				 tostring(unit:name()) == "Idstring(@ID05956ff396f3c58e@)" --[[Blue Package]] or
				 tostring(unit:name()) == "Idstring(@IDc90378ad89058c7d@)" --[[Purple Package]] or
				 tostring(unit:name()) == "Idstring(@ID96504ebd40f8cf98@)" --[[Red Package]] or
				 tostring(unit:name()) == "Idstring(@IDb3cc2abe1734636c@)" --[[Green Package]] ) then
				unit:interaction():interact(managers.player:player_unit())
			end
		end
		--[[Big Oil Day 2]]
		if ( managers.job:current_level_id() == "welcome_to_the_jungle_1" ) then
			if ( tostring(unit:name()) == "Idstring(@ID22c41a3f6d26a887@)" --[[Airplane Keys]] or
				 tostring(unit:name()) == "Idstring(@ID5422d8b99c7c1b57@)" --[[Card]] or
				 tostring(unit:name()) == "Idstring(@ID39a568911e4f1318@)" --[[Generic Intel]] or
				 tostring(unit:name()) == "Idstring(@IDdf59d98261985354@)" --[[Blueprints]] ) then
				if ( legit and unit:interaction():active() or legit == false ) then
					unit:interaction():interact(managers.player:player_unit())
				end
			end
		end
	end
end

--[[* Simple for loop, calls the secure function X number of times passed by the times parameter.]]
function securemult( name, times )
    if ( managers.player:player_unit() ) then
		local i = 1
        while ( i <= times ) do
			secure(name)
            i = i + 1
        end
    end
end

-- HOST CHECK
function isHost()
	if not Network then return false end
	return not Network:is_client()
end

--[[* Forces the success scenario when called.]]
function success()
	if managers.platform:presence() == "Playing" then
		for _, script in pairs(managers.mission:scripts()) do
			for _,element in pairs(script:elements()) do
				local name = element:editor_name()
				if ( string.find(name, "escape") --[[or string.find(name, "point_")]] ) then
					if ( isHost() ) then
						element:on_executed(managers.player:player_unit())
					else
						managers.network:session():send_to_host("to_server_mission_element_trigger", element:id(), managers.player:player_unit())
					end
				end
			end
		end
	end
end

--[[* Unlocks assets at the menu prior to a mission.
    *
    * TODO: Look in to possible forced in-game unlocking and disable broadcasting message of unlocking.]]
function unlock_asset( asset_id )
        local asset = managers.assets:_get_asset_by_id( asset_id )
        if asset then
                if Network:is_server() and not managers.assets:get_asset_triggered_by_id( asset_id ) then
                        managers.assets:server_unlock_asset( asset_id )
                elseif not managers.assets:get_asset_unlocked_by_id( asset_id ) then
                        managers.network:session():send_to_host( "server_unlock_asset", asset_id )
                end
                asset.show = true
                managers.assets:init_finalize()
                managers.menu_component:create_asset_mission_briefing_gui()
        end
end

--[[Main code]]
if managers.job:current_level_id() then
	if managers.job:current_level_id() == "alex_1" then
		if ( legit == true ) then
			securemult("meth",7) --Max legit is 7.
		else
			securemult("meth",8) --The game allows 8.
		end
		success()
	elseif (managers.job:current_level_id() == "framing_frame_1") and (legit == false) then
		securemult("painting",9)
		success()
	elseif managers.job:current_level_id() == "framing_frame_2" then
		local unit_list = World:find_units_quick( "all" )
		for _,unit in ipairs( unit_list ) do
				if ( tostring(unit:name()) == "Idstring(@ID853940b5f91847cf@)" --[[Painting]] ) then
						if ( unit:interaction():active() == true ) then
								secure("money")
						end
				end
		end
		success()
	elseif managers.job:current_level_id() == "framing_frame_3" then
		securemult("gold",8)
		success()
	elseif (managers.job:current_level_id() == "firestarter_1") then
		securemult("weapon",12)
		success()
	elseif managers.job:current_level_id() == "firestarter_3" then
		securemult("money",4)
		securemult("gold",4)
		success()
	elseif managers.job:current_level_id() == "watchdogs_1" then
		getallsecureables()
		--[[TODO: Put code here to fix day 2 not spawning 12 bags.]]
		success()
	elseif managers.job:current_level_id() == "welcome_to_the_jungle_1" then
		getallsecureables()
		success()
	elseif managers.job:current_level_id() == "welcome_to_the_jungle_2" then
		if ( managers.player:player_unit() ) then
			success()
		elseif legit == false then
			unlock_asset("welcome_to_the_jungle_keycard") --!!WARNING!! This now shows you unlocking it at the menu. Be careful with this.
			unlock_asset("welcome_to_the_jungle_plane_keys")
		end
     else --[[Generic map]]
		getallsecureables()
		if( legit == false ) then
			securemult("meth",150)
		end
		success()
     end
end