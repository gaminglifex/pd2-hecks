_determineMoveDirectionOriginal = _determineMoveDirectionOriginal or PlayerStandard._determine_move_direction
function PlayerStandard:_determine_move_direction()
	local interact_expire = self._interact_expire_t
	self._interact_expire_t = false
	_determineMoveDirectionOriginal(self)
	self._interact_expire_t = interact_expire
end