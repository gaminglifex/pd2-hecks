{
	"name": "FPS Fixer",
	"description": "Does OVERKILL's job for them.",
	"author": "gir489",
	"contact": "https://bitbucket.org/gir489/",
	"version": "2",
	"priority": 500,
	"persist_scripts" : [
		{ "global" : "OverkillSucksAtProgramming", "script_path" : "WhyDoIHaveToDoOverkillsJob.lua" }
	]
}