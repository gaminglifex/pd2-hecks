{
	"name": "Kill all Enemies and Civs",
	"description": "Adds a keybind to kill all enemies and civs.",
	"author": "gir489",
	"contact": "https://bitbucket.org/gir489/",
	"version": "4",
	"priority": 500,
	"keybinds": [
		{
			"keybind_id": "KillEnemiesKey",
			"name": "KillEnemies",
			"description": "Key to kill all enemies.",
			"script_path": "enemy.lua",
			"run_in_menu": false,
			"run_in_game": true,
			"localized": false
		},
		{
			"keybind_id": "KillCivsKey",
			"name": "KillCivs",
			"description": "Key to kill all civilians.",
			"script_path": "civs.lua",
			"run_in_menu": false,
			"run_in_game": true,
			"localized": false
		}
	]
}