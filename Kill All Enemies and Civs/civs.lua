function MoneyManager.get_civilian_deduction() return 0 end
function MoneyManager.civilian_killed() return end
function nukeunit(pawn)
	local col_ray = { }
	col_ray.ray = Vector3(1, 0, 0)
	col_ray.position = pawn.unit:position()
	local action_data = {}
	action_data.variant = "explosion"
	action_data.damage = 2000
	action_data.attacker_unit = managers.player:player_unit()
	action_data.col_ray = col_ray
	pawn.unit:character_damage():damage_explosion(action_data)
	local corpseUnitData = managers.enemy:get_corpse_unit_data_from_key(pawn.unit:key())
	if ( corpseUnitData ~= nil ) then
		managers.enemy:remove_corpse_by_id(corpseUnitData.u_id)
	end
end
for u_key,u_data in pairs(managers.enemy:all_civilians()) do
	nukeunit(u_data)
	if Network:is_server() then
		--managers.enemy:remove_corpse_by_id(managers.enemy:get_corpse_unit_data_from_key(u_data.unit:key()).u_id)
	else
		--managers.network:session():send_to_host("sync_interacted_by_id", managers.enemy:get_corpse_unit_data_from_key(u_data.unit:key()).u_id, "corpse_dispose" )
	end
end