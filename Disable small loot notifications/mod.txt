{
    "name" : "Disable small loot notifications",
    "description" : "Disables the text pop-up of the value of the small loot you take.",
    "author" : "gir489"
    "contact": "https://bitbucket.org/gir489/",
    "version" : "1",
    "priority" : 500,
	"hooks" : [
        {"hook_id" : "lib/managers/lootmanager", "script_path" : "lootmanager.lua"}
	]
}