if not _queueDialog then _queueDialog = DialogManager.queue_dialog end
function DialogManager:queue_dialog(id, ...)
	if (managers.job:current_level_id() == "alex_1") or (managers.job:current_level_id() == "rat") then
		if (id == "pln_rt1_22") then
			managers.hud:show_hint( { text = "CS" } )
		elseif (id == "pln_rt1_20") then
			managers.hud:show_hint( { text = "MU" } )
		elseif (id == "pln_rt1_24") then
			managers.hud:show_hint( { text = "HCL" } )
		end
	end
	return _queueDialog(self, id, ...)
end