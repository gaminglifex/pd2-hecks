{
	"name": "Machine Gun Sights",
	"description": "Allows the LMGs to have proper sights.",
	"author": "gir489",
	"contact": "https://bitbucket.org/gir489/",
	"version": "1",
	"priority": 1000,
	"hooks" : [
		{ "hook_id" : "lib/tweak_data/playertweakdata", "script_path" : "playertweakdata.lua" },
		{ "hook_id" : "lib/tweak_data/weaponfactorytweakdata", "script_path" : "weaponfactorytweakdata.lua" }
	]
}