--[[* Machine Gun Sights v1.0 by gir489
	*
	* Changelog:
	*	v1: Initial Release
	*
	* Not for use in Pirate Perfagtion Trainer]]
_initM249 = _initM249 or PlayerTweakData._init_m249
function PlayerTweakData:_init_m249()
	_initM249(self)
	local pivot_shoulder_translation = Vector3( 10.74, -3, -0.5 )
	local pivot_shoulder_rotation = Rotation( 0, 0, 0 )
	local pivot_head_rotation = Rotation( 0, 0, 0 )
	self.stances.m249.steelsight.shoulders.translation = Vector3( 0, 0, 0.2 ) - pivot_shoulder_translation:rotate_with( pivot_shoulder_rotation:inverse() ):rotate_with( pivot_head_rotation )
	self.stances.m249.steelsight.shoulders.rotation = pivot_head_rotation
end

_initRPK = _initRPK or PlayerTweakData._init_rpk
function PlayerTweakData:_init_rpk()
	_initRPK(self)
	local pivot_shoulder_translation = Vector3( 10.64, 23, -5.1 )
	local pivot_shoulder_rotation = Rotation( 0, 0, 0 )
	local pivot_head_rotation = Rotation( 0, 0, -0.5 )
	self.stances.rpk.steelsight.shoulders.translation = Vector3( 0, 0, 0 ) - pivot_shoulder_translation:rotate_with( pivot_shoulder_rotation:inverse() ):rotate_with( pivot_head_rotation )
	self.stances.rpk.steelsight.shoulders.rotation = pivot_head_rotation
end

_initBrenner = _initBrenner or PlayerTweakData._init_hk21
function PlayerTweakData:_init_hk21()
	_initBrenner(self)
	local pivot_shoulder_translation = Vector3( 8.6, 20, -3.4 )
	local pivot_shoulder_rotation = Rotation( 0, 0, 0 )
	local pivot_head_rotation = Rotation( 0, 0, 0 )
	self.stances.hk21.steelsight.shoulders.translation = Vector3( 0.03, 15, 0 ) - pivot_shoulder_translation:rotate_with( pivot_shoulder_rotation:inverse() ):rotate_with( pivot_head_rotation )
	self.stances.hk21.steelsight.shoulders.rotation = pivot_head_rotation
end

_initM240B = _initM240B or PlayerTweakData._init_par
function PlayerTweakData:_init_par()
	_initM240B(self)
	local pivot_shoulder_translation = Vector3( 10.05, -3.7, 0 )
	local pivot_shoulder_rotation = Rotation( 0, -58.75, 0 )
	local pivot_head_rotation = Rotation( 0, 0.2, 0 )
	self.stances.par.steelsight.shoulders.translation = Vector3( 0, 4, .75 ) - pivot_shoulder_translation:rotate_with( pivot_shoulder_rotation:inverse() ):rotate_with( pivot_head_rotation )
	self.stances.par.steelsight.shoulders.rotation = pivot_head_rotation
end