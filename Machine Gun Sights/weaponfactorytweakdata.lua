--[[* Machine Gun Sights v1.0 by gir489
	*
	* Changelog:
	*	v1: Initial Release
	*
	* Not for use in Pirate Perfagtion Trainer]]
_initM249Tweak = _initM249Tweak or WeaponFactoryTweakData._init_m249
function WeaponFactoryTweakData:_init_m249()
	_initM249Tweak(self)
	for _,part in ipairs(self.parts.wpn_fps_shot_r870_s_folding.forbids) do
		if ( part ~= "wpn_fps_shot_r870_ris_special" ) then
			table.insert(self.wpn_fps_lmg_m249.uses_parts, part)
		end
	end
	
end

_initRPKTweak = _initRPKTweak or WeaponFactoryTweakData._init_rpk
function WeaponFactoryTweakData:_init_rpk()
	_initRPKTweak(self)
	for _,part in ipairs(self.parts.wpn_fps_shot_r870_s_folding.forbids) do
		if ( part ~= "wpn_fps_shot_r870_ris_special" ) then
			table.insert(self.wpn_fps_lmg_rpk.uses_parts, part)
		end
	end
end

_initBrennerTweak = _initBrennerTweak or WeaponFactoryTweakData._init_hk21
function WeaponFactoryTweakData:_init_hk21()
	_initBrennerTweak(self)
	for _,part in ipairs(self.parts.wpn_fps_shot_r870_s_folding.forbids) do
		if ( part ~= "wpn_fps_shot_r870_ris_special" ) then
			table.insert(self.wpn_fps_lmg_hk21.uses_parts, part)
		end
	end
end

_initM240BTweak = _initM240BTweak or WeaponFactoryTweakData._init_par
function WeaponFactoryTweakData:_init_par()
	_initM240BTweak(self)
	for _,part in ipairs(self.parts.wpn_fps_shot_r870_s_folding.forbids) do
		if ( part ~= "wpn_fps_shot_r870_ris_special" ) then
			table.insert(self.wpn_fps_lmg_par.uses_parts, part)
		end
	end
end

__initM8Tweak = __initM8Tweak or WeaponFactoryTweakData.init
function WeaponFactoryTweakData:init()
	__initM8Tweak(self)
	for _,part in ipairs(self.parts.wpn_fps_shot_r870_s_folding.forbids) do
		if ( part ~= "wpn_fps_shot_r870_ris_special" ) then
			self.parts[part].stance_mod.wpn_fps_lmg_m249 = { translation = Vector3(0, 0, -3.57) }
			self.parts[part].stance_mod.wpn_fps_lmg_rpk = { translation = Vector3(0, -2, -3.065), rotation = Rotation(-0.05, 0, 0) }
			self.parts[part].stance_mod.wpn_fps_lmg_hk21 = { translation = Vector3(0.01, 1, -3.28) }
			self.parts[part].stance_mod.wpn_fps_lmg_par = { translation = Vector3(0.03, -8, -3.25) }
		end
	end
	--Special cases
	self.parts.wpn_fps_upg_o_acog.stance_mod.wpn_fps_lmg_m249 = { translation = Vector3(0.03, 2, -3.55) }
	self.parts.wpn_fps_upg_o_acog.stance_mod.wpn_fps_lmg_rpk = { translation = Vector3(0.01, 1, -3.065), rotation = Rotation(-0.05, 0, 0) }
	self.parts.wpn_fps_upg_o_acog.stance_mod.wpn_fps_lmg_par = { translation = Vector3(0.03, -5, -3.25) }
end