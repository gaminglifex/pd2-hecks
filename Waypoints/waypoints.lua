-----------------------------------------
---     Waypoints by gir489 v3.15     ---
-----------------------------------------
--Credits: zephirot for Waypoints - All In One v11.2.5.
--         Sirgoodsmoke for an updated version for MVP.
--
--Changelog:    v1.0: Initial Release
--              v1.1: Fixed cash bags on Miami Heat Day 1 being too dark to see.
--                    Fixed error showing in the console when using showHUDMessages = false
--              v1.2: Fixed crash related to showHUDMessages = false when finishing a job.
--              v1.3: Removed duplicate code block for 8th index since we're no longer doing that stupid nonsense. (Forgot to do this in 1.1 and 1.2)
--                    Added Murky Station waypoints.
--                    Refactored the way doors are shown as waypoints.
--                    Refactored thermite waypoint to be on every map.
--                    Added option to show where the camera computers were as a waypoint.
--                    Refactored redundant current_level_id calls with a local level variable.
--              v1.4: Refactored Add/Remove waypoint logic to use a look up table instead of a bunch of or statements.
--                    Fixed some Door waypoints not respecting showDoors.
--                    Added Aftershock waypoints.
--              v1.5: Refactored the entire project to use additive waypoints. This will reduce flicker and increase performance.
--              v1.6: Fixed keycards attached to Civilians not being removed.
--                    Removed unncessary for loop in remove_waypoint.
--                    Fixed some special events in remove_unit callback not properly refreshing.
--                    Fixed torch/thermite not showing if the civilian was frightened or cuffed.
--             v1.61: Reverted Civilian alive check since it didn't work.
--             v1.62: Fixed civilian keycard waypoint not working.
--             v1.63: Fixed civilian keycard waypoint not being removed properly when cuffing them.
--              v1.7: Added drills for the showDrills boolean.
--             v1.71: Fixed chems not working.
--             v1.72: Fixed dockyard causing a crash.
--              v1.8: Fixed Big Bank showing too many keyboards for the host.
--                    Fixed hard drive place location still showing on Murky Station after placing the hard drive.
--                    Fixed Framing Frame Day 3 waypoints not working properly.
--                    Added pig for slaughterhouse.
--              v1.9: Reduxed Alesso heist C4 waypoint in to an X icon where the doors are.
--                    Fixed remove_all_waypoints using a redundant waypoint member when it could've been using it from the for loop variables.
--              v2.0: Added door waypoint to Alesso heist instead of a cross that removes itself when you open the door.
--                    Added Counterfeit waypoints.
--                    Fixed spelling errors in the changelog.
--              v2.1: Added support to see the goats on Day 1 Goat Simulator heist if you are the host.
--            v2.1.1: Added support to see the goats on Day 1 Goat Simulator heist when the user is a client.
--              v2.2: Fixed garbage Meltdown waypoints from predecessor.
--                    Fixed a crash with Big Oil Day 2 when not the host.
--                    Added code to update the waypoint of a vehicle as it moves.
--                    Added more meaningful comments.
--              v2.3: Added support for Beneath The Mountain.
--                    Fixed some waypoints showing as money when they were mission items.
--                    Genericized some waypoints to only look for a partial name.
--              v2.4: Fixed carpet waypoint on Miami Heat Day 1.
--                    Genericized gas can waypoint.
--              v2.5: Fixed Miami Heat Day 1 showing the door and Big Bank showing the keyboard after it was relevant.
--                    Changed some crosshair icons to relevant icons.
--              v2.6: Added support to show where the voting machines for Election Day 2 are at the cost of everyone seeing them.
--                    Added sheaterNewb boolean which will let the user determine if they want to be risque with Goat Simulator/Election Day 2.
--                    Added comments next to the configuration booleans.
--                    Fixed some grammatical errors with the comments and change log.
--              v2.7: Removed The Diamond stepping stone waypoints since they didn't work at all for me.
--                    Refactored some of the logic to ignore ungrabbales from The Diamond.
--                    Fixed a crash when the user was the host on Big Oil Day 2 and they dropped the engine.
--             v2.71: Fixed a crash caused by dead units still being in the _interactive_units array.
--              v2.8: Added Gage Specops Cases and Keys to Gage Packages.
--                    Added support for BLT Mod Options configuration instead of relying on the user to edit the Lua file.
--                    Added waypoints for Stealing Xmas.
--                    Changed Planks to be orange instead of green.
--             v2.81: Added support for the Scarface Heist.
--             v2.82: Removed Big Oil Day 2 waypoints, because they were unreliable, even as host.
--             v2.83: Added giant toothbrush to the waypoints.
--             v2.84: Added waypoints for Heat Street.
--             v3.00: Refactored entity iteration to be more efficent and hopefully fix missing waypoints.
--                    Added waypoints for Green Bridge.
--                    Fixed keycard waypoints not being removed from attached civilians/guards when they RIP in spaghetti noodles microwaved for 10 minutes, then poured on the floor. #ThatsFuckingMetalDude #IRespectYouDawg
--                    Added sheaterNewb scenario for Firestarter day 2's boxes.
--                    Reworked all of the mission script-relevant waypoints as host to be removed once that mission item has been completed.
--                    Fixed Goat Simulator heist sheaterNewb scenario highlighting random boxes.
--                    Reworked Transport: Train Heist waypoints to use the hard drive icon for the location of valid drill spots, and to also not to show irrelevant camera computer positions.
--                    Reworked Hoxton Breakout Day 1 to show the correct keyboard for both host and client.
--             v3.01: Fixed nukes not being shown in Meltdown.
--                    Fixed some waypoints being shown in the incorrect location.
--             v3.10: Fixed a crash caused by using unit_data instead of interaction when assigning waypoint_id to the unit.
--             v3.15: Reworked cop/civ keycard code to not crash and to only remove the single cop/civ waypoint attached to the card dropped.
--                    Changed the locked doors waypoint icon to be less of an eyesore.
--                    Changed the ammo on train heist to use the ammo bag icon instead of the sentry icon.
--             v3.16: Fixed waypoints for First World Bank.

--Define various colors
local alpha_value   = 200    -- (0->255)
local white         = Color(         100, 255, 255, 255 ) / 255
local bone_white    = Color( alpha_value, 255, 238, 151 ) / 255
local magenta       = Color( alpha_value, 255, 000, 255 ) / 255
local purple        = Color( alpha_value, 154, 068, 220 ) / 255
local matte_purple  = Color( alpha_value, 107, 084, 144 ) / 255
local pink          = Color( alpha_value, 255, 122, 230 ) / 255
local light_gray    = Color( alpha_value, 191, 191, 191 ) / 255
local gray          = Color( alpha_value, 128, 128, 128 ) / 255
local dark_gray     = Color( alpha_value, 064, 064, 064 ) / 255
local orange        = Color( alpha_value, 255, 094, 015 ) / 255
local light_brown   = Color( alpha_value, 204, 115, 035 ) / 255
local bright_yellow = Color( alpha_value, 255, 207, 076 ) / 255
local brown         = Color( alpha_value, 128, 070, 013 ) / 255
local gold          = Color(         255, 255, 215, 000 ) / 255
local yellow        = Color( alpha_value, 255, 255, 000 ) / 255
local warm_yellow   = Color( alpha_value, 250, 157, 007 ) / 255
local red           = Color( alpha_value, 255, 000, 000 ) / 255
local blood_red     = Color( alpha_value, 138, 017, 009 ) / 255
local coral_red     = Color( alpha_value, 213, 036, 053 ) / 255
local dark_red      = Color( alpha_value, 110, 015, 022 ) / 255
local blue          = Color( alpha_value, 000, 000, 255 ) / 255
local gray_blue     = Color( alpha_value, 012, 068, 084 ) / 255
local navy_blue     = Color( alpha_value, 040, 052, 086 ) / 255
local matte_blue    = Color( alpha_value, 056, 097, 168 ) / 255
local light_blue    = Color( alpha_value, 126, 198, 238 ) / 255
local cobalt_blue   = Color( alpha_value, 000, 093, 199 ) / 255
local turquoise     = Color( alpha_value, 000, 209, 157 ) / 255
local cyan          = Color( alpha_value, 000, 255, 255 ) / 255
local green         = Color( alpha_value, 000, 255, 000 ) / 255
local lime_green    = Color( alpha_value, 000, 166, 081 ) / 255
local leaf_green    = Color( alpha_value, 104, 191, 054 ) / 255
local olive_green   = Color( alpha_value, 072, 090, 050 ) / 255
local dark_green    = Color( alpha_value, 007, 061, 009 ) / 255
local toxic_green   = Color( alpha_value, 167, 248, 087 ) / 255

-- INGAME CHECK
function inGame()
	if not game_state_machine then return false end
	return string.find(game_state_machine:current_state_name(), "game")
end

-- IS PLAYING CHECK
function isPlaying()
	if not BaseNetworkHandler then return false end
	return BaseNetworkHandler._gamestate_filter.any_ingame_playing[ game_state_machine:last_queued_state_name() ]
end

-- HOST CHECK
function isHost()
	if not Network then return false end
	return not Network:is_client()
end

-- CLIENT CHECK
function isClient()
	if not Network then return false end
	return Network:is_client()
end

-- BEEP
function beep()
	if managers and managers.menu_component and Waypoints.settings["makeNoise"] then
		managers.menu_component:post_event("menu_enter")
	end
end

function set_waypoint_color(waypoint, waypoint_id)
	-- BASE COLOR
	if waypoint_id:sub(1,10) == 'hudz_base_' or waypoint_id:sub(1,9) == 'hudz_car_' then
		waypoint.bitmap:set_color(white)
	end
	-- KEYCARD FLOOR
	if waypoint_id:sub(1,9) == 'hudz_key_' then
		waypoint.bitmap:set_color( yellow )
	end
	-- KEYCARD CIV
	if waypoint_id:sub(1,9) == 'hudz_civ_' then
		waypoint.bitmap:set_color( orange )
	end
	-- KEYCARD COP
	if waypoint_id:sub(1,9) == 'hudz_cop_' then
		waypoint.bitmap:set_color( cobalt_blue )
	end
	-- WEAPON
	if waypoint_id:sub(1,9) == 'hudz_wpn_' then
		waypoint.bitmap:set_color( magenta )
	end
	-- GOLD/JEWEL
	if waypoint_id:sub(1,10) == 'hudz_gold_' then
		waypoint.bitmap:set_color( gold )
	end
	-- SMALL LOOT
	if waypoint_id:sub(1,10) == 'hudz_cash_' then
		waypoint.bitmap:set_color( dark_green )
	end
	-- MONEY (BAG)
	if waypoint_id:sub(1,11) == 'hudz_cashB_' then
		waypoint.bitmap:set_color( green )
	end
	-- PAINTING
	if waypoint_id:sub(1,9) == 'hudz_ptn_' then
		waypoint.bitmap:set_color( green )
	end
	-- PACKAGE
	if waypoint_id:sub(1,10) == 'hudz_pkgY_' then
		waypoint.bitmap:set_color( yellow )
	elseif waypoint_id:sub(1,10) == 'hudz_pkgB_' then
		waypoint.bitmap:set_color( blue )
	elseif waypoint_id:sub(1,10) == 'hudz_pkgP_' then
		waypoint.bitmap:set_color( magenta )
	elseif waypoint_id:sub(1,10) == 'hudz_pkgR_' then
		waypoint.bitmap:set_color( red )
	elseif waypoint_id:sub(1,10) == 'hudz_pkgG_' then
		waypoint.bitmap:set_color( green )
	end
	-- PLANK
	if waypoint_id:sub(1,11) == 'hudz_drill_' then
		waypoint.bitmap:set_color( green )
	end
	if waypoint_id:sub(1,9) == 'hudz_plk_' then
		waypoint.bitmap:set_color( orange )
	end
	-- METHLAB
	if waypoint_id:sub(1,11) == 'hudz_coke1_' then
		waypoint.bitmap:set_color( white )
	elseif waypoint_id:sub(1,11) == 'hudz_coke2_' then
		waypoint.bitmap:set_color( green )
	elseif waypoint_id:sub(1,11) == 'hudz_coke3_' then
		waypoint.bitmap:set_color( yellow )
	end
	-- ATM
	if waypoint_id:sub(1,9) == 'hudz_atm_' then
		waypoint.bitmap:set_color( blood_red )
	end
	-- DOOR
	if waypoint_id:sub(1,10) == 'hudz_door_' then
		waypoint.bitmap:set_color( cyan )
	end
	if waypoint_id:sub(1,10) == 'hudz_Robj_' then
		waypoint.bitmap:set_color( green )
	end
	if waypoint_id:sub(1,10) == 'hudz_Wobj_' then
		waypoint.bitmap:set_color( red )
	end
end

function add_waypoint_npc(unit, prefab, appendage, icon)
	if (unit:unit_data()._waypoint_id == nil ) then
		local waypoint_id = prefab .. appendage
		local position = unit:movement():m_head_pos()
		unit:unit_data()._waypoint_id = tostring(waypoint_id)
		managers.hud:add_waypoint( unit:unit_data()._waypoint_id, { icon = icon, distance = Waypoints.settings["showDistance"], position = position, no_sync = true,  present_timer = 0, state = "present", radius = 800, color = Color.orange, blend_mode = "add" }  )
		local waypoint_id = unit:unit_data()._waypoint_id
		local waypoint = managers.hud._hud.waypoints[waypoint_id]
		if ( waypoint ) then
			waypoint.npc_unit = unit
			waypoint.move_speed = 0
			set_waypoint_color(waypoint, waypoint_id)
		end
	end
end

function add_waypoint(unit, prefab, appendage, icon)
	if ( unit:interaction()._waypoint_id == nil ) then
		local waypoint_id = prefab .. appendage
		local position = unit:interaction():interact_position()
		unit:interaction()._waypoint_id = tostring(waypoint_id)
		managers.hud:add_waypoint( unit:interaction()._waypoint_id, { icon = icon, distance = Waypoints.settings["showDistance"], position = position, no_sync = true, present_timer = 0, state = "present", radius = 800, color = Color.white, blend_mode = "add" }  )
		local waypoint = managers.hud._hud.waypoints[waypoint_id]
		if ( waypoint ) then
			waypoint._unit = unit
			waypoint.move_speed = 0
			set_waypoint_color(waypoint, waypoint_id)
		end
	end
end

function remove_waypoint(unit)
	local waypoint_id = unit:interaction()._waypoint_id
	local waypoint = managers.hud._hud.waypoints[waypoint_id]
	if ( waypoint ) then
		waypoint._unit:interaction()._waypoint_id = nil
		managers.hud:remove_waypoint( waypoint_id )
	end
end

function remove_all_waypoints()
	for id,waypoint in pairs( managers.hud._hud.waypoints ) do
		id = tostring(id)
		if id:sub(1,5) == 'hudz_' then
			if ( waypoint._unit ) then
				waypoint._unit:interaction()._waypoint_id = nil
			elseif ( waypoint.npc_unit ) then
				waypoint.npc_unit:unit_data()._waypoint_id = nil
			end
			managers.hud:remove_waypoint( id )
		end
	end
end

function find_associated_npc_waypoint(keycard_unit)
	for id, waypoint in pairs( managers.hud._hud.waypoints ) do
		local waypoint_id = tostring(id)
		if ( waypoint_id:sub(1,9) == 'hudz_civ_' or waypoint_id:sub(1,9) == 'hudz_cop_' ) then
			local position = keycard_unit:position() - waypoint.position
			if ( position:length() < 200 ) then
				managers.hud:remove_waypoint( id )
			end
		end
	end
end

function RefreshMsg()
	local level = managers.job:current_level_id()
	if _toggleWaypointAIO2 > 0 and Waypoints.settings["showHUDMessages"] == true then
		local xval = RenderSettings.resolution.x
		local yval = RenderSettings.resolution.y
		local txtsize = 10
		if xval >= 600 and xval < 800 then txtsize = 10 elseif xval >= 800 and xval < 1024 then txtsize = 15 elseif xval >= 1024 and xval < 1280 then txtsize = 20 elseif xval >= 1280 and xval < 1360 then txtsize = 25 elseif xval >= 1360 and xval < 1440 then txtsize = 30 elseif xval >= 1440 and xval < 1600 then txtsize = 35 elseif xval >= 1600 and xval < 1920 then txtsize = 40 elseif xval >= 1920 and xval < 2400 then	txtsize = 45 else txtsize = 40 end
		if not _HDmsg then
			_HDmsg = {}
			_HDmsg.ws = Overlay:newgui():create_screen_workspace()
			_HDmsg.lbl = _HDmsg.ws:panel():text{ name="lbl_1", x = -(RenderSettings.resolution.x / 2.1) + 0.5 * RenderSettings.resolution.x, y = -(RenderSettings.resolution.y / 4) + 6.1/9 * RenderSettings.resolution.y, text = "", font = tweak_data.menu.pd2_large_font, font_size = txtsize, color = Color.white:with_alpha(0.7), layer = 1 }
			_HDmsg.lbl2 = _HDmsg.ws:panel():text{ name="lbl_2", x = -(RenderSettings.resolution.x / 2.1) + 0.5 * RenderSettings.resolution.x, y = -(RenderSettings.resolution.y / 4) + 5.6/9 * RenderSettings.resolution.y, text = "Waypoint", font=tweak_data.menu.pd2_large_font, font_size = txtsize, color = Color.green:with_alpha(0.7), layer = 1 }
			_HDmsg.lbl:show()
			_HDmsg.lbl2:show()
		end
		local mess = ""
		if _toggleWaypointAIO2 == 1 then
			mess = "[1/8] PACKAGES"
			_HDmsg.lbl:set_text(mess)
		elseif _toggleWaypointAIO2 == 2 then
			mess = "[2/8] KEYCARD/SECURITY DOOR"
			_HDmsg.lbl:set_text(mess)
		elseif _toggleWaypointAIO2 == 3 then
			mess = "[3/8] WEAPON/COKE"
			_HDmsg.lbl:set_text(mess)
		elseif _toggleWaypointAIO2 == 4 then
			mess = "[4/8] PLANK/DRILL"
			_HDmsg.lbl:set_text(mess)
		elseif _toggleWaypointAIO2 == 5 then
			mess = "[5/8] CRATE/CROWBAR"
			_HDmsg.lbl:set_text(mess)
		elseif _toggleWaypointAIO2 == 6 then
			mess = "[6/8] MISSION"
			_HDmsg.lbl:set_text(mess)
		elseif _toggleWaypointAIO2 == 7 then
			mess = "[7/8] MONEY"
			_HDmsg.lbl:set_text(mess)
		elseif _toggleWaypointAIO2 == 8 then
			mess = "[8/8] ALL"
			_HDmsg.lbl:set_text(mess)
		end
	end
end

if inGame() and isPlaying() then
	local level = managers.job:current_level_id()
	--Setup custom icons for extra 420 dank memeage. (IF you know what I meme.)
	if ( tweak_data.hud_icons.pd2_nuke == nil ) then
		tweak_data.hud_icons.pd2_nuke = {
			texture = "guis/textures/pd2/lootscreen/loot_cards",
			texture_rect = {
				48,
				56,
				30,
				66
			}
		}
		tweak_data.hud_icons.equipment_handcuffs = {
		texture = "guis/textures/hud_icons",
		texture_rect = {
			56,
			201,
			32,
			32
			}
		}
	end
	if not _toggleWaypointAIO2 then _toggleWaypointAIO2 = 0 end
	if Waypoints.settings["iterativeToggle"] == false then
		_toggleWaypointAIO2 = _toggleWaypointAIO2 ~= 8 and 8 or 0
	else
		_toggleWaypointAIO2 = _toggleWaypointAIO2 == 8 and 0 or _toggleWaypointAIO2 + 1
	end
	cm = managers.controller
	if (Waypoints.settings["showHUDMessages"] == true) then
		if not _HDmsg then
			_HDmsg = {}
			_HDmsg.ws = Overlay:newgui():create_screen_workspace()
			local xval = RenderSettings.resolution.x
			local yval = RenderSettings.resolution.y
			local txtsize = 10
			if xval >= 600 and xval < 800 then txtsize = 10 elseif xval >= 800 and xval < 1024 then txtsize = 15 elseif xval >= 1024 and xval < 1280 then txtsize = 20 elseif xval >= 1280 and xval < 1360 then txtsize = 25 elseif xval >= 1360 and xval < 1440 then txtsize = 30 elseif xval >= 1440 and xval < 1600 then txtsize = 35 elseif xval >= 1600 and xval < 1920 then txtsize = 40 elseif xval >= 1920 and xval < 2400 then	txtsize = 45 else txtsize = 40 end
			_HDmsg.lbl = _HDmsg.ws:panel():text{ name="lbl_1", x = -(RenderSettings.resolution.x / 2.1) + 0.5 * RenderSettings.resolution.x, y = -(RenderSettings.resolution.y / 4) + 6.1/9 * RenderSettings.resolution.y, text = "", font = tweak_data.menu.pd2_large_font, font_size = txtsize, color = Color.white:with_alpha(0.7), layer = 1 }
			_HDmsg.lbl2 = _HDmsg.ws:panel():text{ name="lbl_2", x = -(RenderSettings.resolution.x / 2.1) + 0.5 * RenderSettings.resolution.x, y = -(RenderSettings.resolution.y / 4) + 5.6/9 * RenderSettings.resolution.y, text = "Waypoint", font=tweak_data.menu.pd2_large_font, font_size = txtsize, color = Color.green:with_alpha(0.7), layer = 1 }
			_HDmsg.lbl3 = _HDmsg.ws:panel():text{ name="lbl_3", x = -(RenderSettings.resolution.x / 2.1) + 0.5 * RenderSettings.resolution.x, y = -(RenderSettings.resolution.y / 4) + 6.6/9 * RenderSettings.resolution.y, text = "", font=tweak_data.menu.pd2_large_font, font_size = txtsize, color = Color.white:with_alpha(0.7), layer = 1 }
			_HDmsg.lbl4 = _HDmsg.ws:panel():text{ name="lbl_4", x = -(RenderSettings.resolution.x / 2.1) + 0.5 * RenderSettings.resolution.x, y = -(RenderSettings.resolution.y / 4) + 7.1/9 * RenderSettings.resolution.y, text = "", font=tweak_data.menu.pd2_large_font, font_size = txtsize, color = Color.red:with_alpha(0.7), layer = 1 }
			_HDmsg.lbl:show()
			_HDmsg.lbl2:show()
			_HDmsg.lbl3:show()
			_HDmsg.lbl4:show()
		end
	end
	keyboard = Input:keyboard()
	if keyboard:pressed() ~= nil then
		beep()
	end

	--Do time-based code for the waypoints here.
	managers.hud.__update_waypoints = managers.hud.__update_waypoints or managers.hud._update_waypoints
	function HUDManager:_update_waypoints( t, dt )
		local result = self:__update_waypoints(t,dt)
		for id, data in pairs( self._hud.waypoints ) do
			id = tostring(id)
			if ( id:sub(1,9) == 'hudz_car_' ) then
				data.position = data._unit:interaction():interact_position() --[[This will keep the waypoint on the vehicle]]
			end
			----------- FRAME COLORED WAYPOINTS ---------------
			-- COKE
			if id:sub(1,10) == 'hudz_coke_' then
				local LSD = Color(1,math.sin(140 * os.clock() + 0) / 2 + 0.5, math.sin(140 * os.clock() + 60) / 2 + 0.5, math.sin(140 * os.clock() + 120) / 2 + 0.5)
				data.bitmap:set_color( LSD )
			end
			-- THERMITE/GASCAN
			if id:sub(1,10) == 'hudz_fire_' then
				local FIRE = Color(1,math.sin(135 * os.clock() + 0) / 2 + 1.5, math.sin(140 * os.clock() + 60) / 2 + 0.5, 0)
				data.bitmap:set_color( FIRE )
			end
		end
		return result
	end

	function RefreshItemWaypoints(doRemove)
		if inGame() and isPlaying() then
			if ( doRemove ) then
				remove_all_waypoints()
			end
			if _toggleWaypointAIO2 > 0 then
				local level = managers.job:current_level_id()
				if ( _missionId == nil ) then
					_missionId = 0
				end
				for key,_ in pairs(managers.objectives:get_active_objectives()) do
					for numberMatch in key:gmatch("%d+") do --Only match last number in the string, which is usually the mission ID.
						_missionId = tonumber(numberMatch)
					end
				end
				if _toggleWaypointAIO2 == 2 or _toggleWaypointAIO2 == 8 then
					-- CIVS WITH KEYCARDS	
					for u_key, u_data in pairs(managers.enemy:all_civilians()) do
						if isHost() and u_data.unit.contour and alive(u_data.unit) and u_data.unit:character_damage():pickup() then
							if (not u_data.unit:character_damage() or not u_data.unit:character_damage():dead()) then
								add_waypoint_npc( u_data.unit, 'hudz_civ_', tostring(u_data.unit:position()), level == "jolly" and 'equipment_briefcase' or 'equipment_bank_manager_key' )
							end
						end
					end
					-- COPS WITH KEYCARDS
					for u_key, u_data in pairs(managers.enemy:all_enemies()) do
						if isHost() and u_data.unit.contour and alive(u_data.unit) and u_data.unit:character_damage():pickup() and u_data.unit:character_damage():pickup() ~= "ammo" then
							if (not u_data.unit:character_damage() or not u_data.unit:character_damage():dead()) then
								add_waypoint_npc( u_data.unit, 'hudz_cop_', tostring(u_data.unit:position()), 'equipment_bank_manager_key' )
							end
						end
					end
				end
				-- FAWKA U MADA U SHEATER NEWB U GO HOME YANKEE CHEESEBURGER BUSH!!!!!!!1111oneoneoneoneshift+1
				if _toggleWaypointAIO2 == 6 or _toggleWaypointAIO2 == 8 and Waypoints.settings["sheaterNewb"] then
					if ( level == 'peta' ) then
						local mission_script = managers.mission:script("default")._elements[100673]
						if ( isHost() ) then
							mission_script:on_executed(managers.player:player_unit())
						else
							managers.network:session():send_to_host("to_server_mission_element_trigger", mission_script:id(), nil)
						end
					end
					if ( level == 'election_day_2' ) then
						for _, script in pairs(managers.mission:scripts()) do
							for _,element in pairs(script:elements()) do
								local name = element:editor_name()
								if ( string.find(name, "voting_machine_crate_opened" ) ) then
									--This mission script has code to unhide the voting machines.
									if ( isHost() ) then
										element:on_executed(managers.player:player_unit())
									else
										managers.network:session():send_to_host("to_server_mission_element_trigger", element:id(), managers.player:player_unit())
									end
								end
							end
						end
					end
					if level == "arm_for" then
						local vault1Search = World:find_units_quick( "sphere", Vector3(-1707, -1157, 667), 200, managers.slot:get_mask("all") )
						local found1 = false
						for foundKey,foundUnit in ipairs( vault1Search ) do
							if ( tostring(foundUnit:name()) == 'Idstring(@ID0018274d196d8432@)' and foundUnit:rotation() == Rotation(90, -0, -0) ) then
								found1 = true
							end
						end
						local locationToSearch1 = found1 == false and Vector3(-1707, -1157, 667) or Vector3(-2710, -1152, 666)
						vault1Search = World:find_units_quick( "sphere", locationToSearch1, 200, managers.slot:get_mask("all") )
						for foundKey,foundUnit in ipairs( vault1Search ) do
							if ( foundUnit:interaction() and foundUnit:interaction().tweak_data == 'pickup_harddrive' ) then
								add_waypoint(foundUnit, 'hudz_Robj_', 'vault1', 'equipment_harddrive')
							end
						end
						local vault2Search = World:find_units_quick( "sphere", Vector3(-192, -1152, 668), 200, managers.slot:get_mask("all") )
						local found2 = false
						for foundKey,foundUnit in ipairs( vault2Search ) do
							if ( tostring(foundUnit:name()) == 'Idstring(@ID0018274d196d8432@)' and foundUnit:rotation() == Rotation(90, -0, -0) ) then
								found2 = true
							end
						end
						local locationToSearch2 = found2 == false and Vector3(-192, -1152, 668) or Vector3(794, -1161, 668)
						vault2Search = World:find_units_quick( "sphere", locationToSearch2, 200, managers.slot:get_mask("all") )
						for foundKey,foundUnit in ipairs( vault2Search ) do
							if ( foundUnit:interaction() and foundUnit:interaction().tweak_data == 'pickup_harddrive' ) then
								add_waypoint(foundUnit, 'hudz_Robj_', 'vault2', 'equipment_harddrive')
							end
						end
						local vault3Search = World:find_units_quick( "sphere", Vector3(2291, -1155, 667), 200, managers.slot:get_mask("all") )
						local found3 = false
						for foundKey,foundUnit in ipairs( vault3Search ) do
							if ( tostring(foundUnit:name()) == 'Idstring(@ID0018274d196d8432@)' and foundUnit:rotation() == Rotation(90, -0, -0) ) then
								found3 = true
							end
						end
						local locationToSearch3 = found3 == false and Vector3(2291, -1155, 667) or Vector3(3308, -1151, 667)
						vault3Search = World:find_units_quick( "sphere", locationToSearch3, 200, managers.slot:get_mask("all") )
						for foundKey,foundUnit in ipairs( vault3Search ) do
							if ( foundUnit:interaction() and foundUnit:interaction().tweak_data == 'pickup_harddrive' ) then
								add_waypoint(foundUnit, 'hudz_Robj_', 'vault3', 'equipment_harddrive')
							end
						end
					end
					--Host only access mission script waypoints.
					if isHost() then
						if level == 'framing_frame_3' then
							local serverVectors = { ["105507"] = Vector3(-3937.26, 5644.73, 3474.5),["105508"] = Vector3(-3169.57, 4563.03, 3074.5), ["100650"] = Vector3(-4920, 3737, 3074.5) }
							local serverId = tostring(managers.mission:script("default")._elements[105506]._values.on_executed[1].id)
							local foundUnits = World:find_units_quick( "sphere", serverVectors[serverId], 100, managers.slot:get_mask("all") )
							for foundKey,foundUnit in ipairs( foundUnits ) do
								if ( foundUnit:interaction() and foundUnit:interaction().tweak_data == "big_computer_hackable" ) then
									add_waypoint(foundUnit, 'hudz_Robj_', foundKey, 'interaction_keyboard')
								end
							end
						end
						-- CAR SHOP
						if level == 'cage' then
							local keyboardVectors = { ["104797"] = Vector3(2465.98, 660.75, -149.996), ["104804"] = Vector3(2615.98, 660.75, -149.996), ["104811"] = Vector3(2890.98, 660.75, -149.996), ["104818"] = Vector3(3040.98, 660.75, -149.996), ["104826"] = Vector3(3045.98, 405.75, -149.996), ["104833"] = Vector3(2887.98, 407.75, -149.996), ["104841"] = Vector3(2615.98, 410.75, -149.996), ["104848"] = Vector3(2465.98, 407.75, -149.996), ["104857"] = Vector3(1077.98, 255.751, 250.004), ["104866"] = Vector3(924.978, 255.75, 250.004), ["104873"] = Vector3(617.978, 255.75, 250.004), ["104880"] = Vector3(468.978, 255.749, 250.004), ["104887"] = Vector3(423.024, 142.249, 250.004), ["104899"] = Vector3(590.024, 142.25, 250.004), ["104907"] = Vector3(880.024, 142.25, 250.004), ["104919"] = Vector3(1049.02, 142.251, 250.004), ["104927"] = Vector3(254.75, -1490.98, 249.503) }
							local keyboardId = tostring(managers.mission:script("default")._elements[104929]._values.on_executed[1].id)
							local foundUnits = World:find_units_quick( "sphere", keyboardVectors[keyboardId], 100, managers.slot:get_mask("all") )
							for foundKey,foundUnit in ipairs( foundUnits ) do
								if ( foundUnit:interaction() and foundUnit:interaction().tweak_data == "security_station_keyboard" and (_missionId < 5 or foundUnit:interaction()._active == true) ) then
									add_waypoint(foundUnit, 'hudz_Robj_', foundKey, 'interaction_keyboard')
								end
							end
						end
						if level == 'big' and _missionId and (_missionId == 28 or _missionId < 4) then
							local big1 = tostring(managers.mission:script("default")._elements[103246]._values.on_executed[1].id)
							local stfcmpts = { ["103250"] = Vector3(2754, 1420, -923), ["103229"] = Vector3(2083, 1412, -922.772), ["103569"] = Vector3(1941, 1345, -922.772), ["103604"] = Vector3(1589, 1419, -922.772), ["103647"] = Vector3(2558, 1847, -922.772), ["103709"] = Vector3(2448.08, 1849.07, -922.772), ["103749"] = Vector3(1859.2, 1832.25, -922.772), ["103788"] = Vector3(1732, 1812, -923), ["103898"] = Vector3(1090, 1220, -522.772), ["103916"] = Vector3(1293.46, 1221.04, -522.772), ["103927"] = Vector3(1909, 1389, -522.762), ["103948"] = Vector3(1917.69, 1583.79, -522.762), ["103966"] = Vector3(2318, 1608, -522.762), ["103984"] = Vector3(2319.79, 1407.8, -522.762), ["104006"] = Vector3(2716, 1220, -522.772), ["104024"] = Vector3(2895.76, 1782.56, -522.772), ["104042"] = Vector3(2922, 1218.89, -522.772), ["104080"] = nil, ["104127"] = nil, ["104315"] = nil }
							if tostring(stfcmpts[big1]) ~= 'nil' then
								local foundUnits = World:find_units_quick( "sphere", stfcmpts[big1], 100, managers.slot:get_mask("all") )
								for foundKey,foundUnit in ipairs( foundUnits ) do
									if ( foundUnit:interaction() and foundUnit:interaction().tweak_data == "big_computer_hackable" ) then
										add_waypoint(foundUnit, 'hudz_Robj_', foundKey, 'interaction_keyboard')
									end
								end
							end
						end
						if level == 'firestarter_2' and managers.groupai:state():whisper_mode() then
							local bo_boxes = { ["105819"] = Vector3(-2710, -2830, 552), ["105794"] = Vector3(-1840, -3195, 552), ["105810"] = Vector3(-1540, -2195, 552), ["105824"] = Vector3(-1005, -3365, 552), ["105837"] = Vector3(-635, -1705, 552), ["105851"] = Vector3(-1095, -210, 152), ["106183"] = Vector3(-1230, 1510, 152), ["106529"] = Vector3(-1415, -795, 152), ["106543"] = Vector3(-1160, 395, 152), ["106556"] = Vector3(-5, 735, 152),  ["106594"] = Vector3(795, -898, 552), ["106607"] = Vector3(795, -3240, 552), ["106620"] = Vector3(1060, -2195, 552), ["106633"] = Vector3(204, 540, 578), ["106646"] = Vector3(-1085, -1205, 552), ["106659"] = Vector3(-2135, 395, 552), ["106672"] = Vector3(-2405, -840, 552), ["106685"] = Vector3(-2005, -1640, 552), ["106698"] = Vector3(-2715, -1595, 552), ["106711"] = Vector3(-500, -650, 1300), ["106724"] = Vector3(-400, -650, 1300), ["106737"] = Vector3(-300, -650, 1300), ["106750"] = Vector3(-200, -650, 1300), ["106763"] = Vector3(-100, -650, 1300), ["106776"] = Vector3(-635, -1205, 152), ["106789"] = Vector3(-1040, -95, 552), ["106802"] = Vector3(615, 395, 152), ["106815"] = Vector3(1890, -1805, 152), ["106828"] = Vector3(215, -1805, 152) }
							local SecBox1 = tostring(managers.mission:script("default")._elements[106836]._values.on_executed[1].id)
							local SecBox2 = tostring(managers.mission:script("default")._elements[106836]._values.on_executed[2].id)
							local foundUnits = World:find_units_quick( "sphere", bo_boxes[SecBox1], 100, managers.slot:get_mask("all") )
							for foundKey,foundUnit in ipairs( foundUnits ) do
								if ( foundUnit:interaction() and foundUnit:interaction().tweak_data == "hospital_security_cable" ) then
									add_waypoint(foundUnit, 'hudz_Robj_', foundKey, 'pd2_wirecutter')
								end
							end
							foundUnits = World:find_units_quick( "sphere", bo_boxes[SecBox2], 100, managers.slot:get_mask("all") )
							for foundKey,foundUnit in ipairs( foundUnits ) do
								if ( foundUnit:interaction() and foundUnit:interaction().tweak_data == "hospital_security_cable" ) then
									add_waypoint(foundUnit, 'hudz_Robj_', foundKey, 'pd2_wirecutter')
								end
							end
						end
						if level == 'election_day_1' then
							managers.network:session():send_to_host("to_server_mission_element_trigger", mission_script:id(), nil)
							local truck_vectors = { ["100636"] = Vector3(150, -3900, 0), ["100633"] = Vector3(878.392, -3360.24, 0), ["100637"] = Vector3(149.999, -2775, 0), ["100634"] = Vector3(828.07, -2222.45, 0), ["100639"] = Vector3(149.998, -1625, 0), ["100635"] = Vector3(848.961, -1084.9, 0) }
							local truckv = tostring(managers.mission:script("default")._elements[100631]._values.on_executed[1].id) --pickTruck
							local foundUnits = World:find_units_quick( "sphere", truck_vectors[truckv], 100, managers.slot:get_mask("all") )
							for foundKey,foundUnit in ipairs( foundUnits ) do
								if ( foundUnit:interaction() and foundUnit:interaction():active() and foundUnit:interaction().tweak_data == "hold_place_gps_tracker" ) then
									add_waypoint(foundUnit, 'hudz_Robj_', foundKey, 'equipment_ecm_jammer')
								end
							end
						end
					elseif Waypoints.settings["sheaterNewb"] then
						if level == 'firestarter_2' and managers.groupai:state():whisper_mode() then
							managers.network:session():send_to_host("to_server_mission_element_trigger", 103514, managers.player:player_unit())
						end
					end
				end
				local carpetAlreadyFound = false
				for key, unit in ipairs( World:find_units_quick( "all" ) ) do
					if _toggleWaypointAIO2 == 2 or _toggleWaypointAIO2 == 8 then
						local nameString = tostring(unit:name())
						if ( nameString == "Idstring(@ID5422d8b99c7c1b57@)" or nameString == "Idstring(@ID8a91392271626301@)" or  --Normal Keycard
							 nameString == "Idstring(@ID7778a17af629d64c@)" --[[Keycard A]] or nameString == "Idstring(@IDd05a7d53ca94e597@)" --[[Keycard B]] ) then 
							if ( unit:interaction()._is_selected == nil and (unit:interaction():active() or level == 'dark') ) then
								if level == 'roberts' and unit:position() == Vector3(250, 6750, -64.2354) then
								elseif level == 'big' and unit:position() == Vector3(3000, -3500, 949.99) then
								elseif level == 'firestarter_2' and unit:position() == Vector3(-1800, -3600, 400) then
								else
									add_waypoint(unit, 'hudz_key_', key, 'equipment_bank_manager_key')
								end
							end
						end
					end
					if _toggleWaypointAIO2 == 6 or _toggleWaypointAIO2 == 8 then
						--HOXTON BREAKOUT DAY 1 BOLLARDS COMPUTER
						if ( level == 'hox_1' ) then
							if ( tostring(unit:name()) == 'Idstring(@IDfc4ce94e587a7516@)' ) then
								local foundUnits = World:find_units_quick( "sphere", unit:position(), 200, managers.slot:get_mask("all") )
								for foundKey,foundUnit in ipairs( foundUnits ) do
									if ( foundUnit:interaction() and foundUnit:interaction().tweak_data == 'security_station_keyboard' ) then
										add_waypoint(foundUnit, 'hudz_Robj_', foundKey, 'interaction_keyboard')
									end
								end
							end
						end
						--HOTLINE DAY 1 CARPETED DOOR
						if ( level == 'mia_1' and _missionId ) then
							if ( _missionId == 19 or _missionId < 6 ) then
								if (unit:interaction() and unit:interaction().tweak_data == "hlm_roll_carpet" ) then
									carpetAlreadyFound = true
								end
								if ( tostring(unit:name()) == "Idstring(@ID0ff3ba27d5862ba2@)" and carpetAlreadyFound == false ) then --Hatch
									local foundUnits = World:find_units_quick( "sphere", unit:position(), 200, managers.slot:get_mask("all") )
									for foundKey,foundUnit in ipairs( foundUnits ) do
										if ( tostring(foundUnit:name()) == "Idstring(@ID2c1e5738c0ad2f85@)") then --Door
											managers.hud:add_waypoint( 'hudz_base_hlm1door', { icon = 'pd2_door', distance = Waypoints.settings["showDistance"], position = foundUnit:position(), no_sync = true, present_timer = 0, state = "present", radius = 800, color = Color.white, blend_mode = "add" }  )
											local waypoint = managers.hud._hud.waypoints['hudz_base_hlm1door']
											waypoint.move_speed = 0
										end
									end
								end
							end
						end
						--BENEETH THE MOUTIAN
						if ( level == 'pbr' ) then
							if ( tostring(unit:name()) == "Idstring(@IDf11234ccd3e2d814@)") then --Bars in front of the paintings
								local foundUnits = World:find_units_quick( "sphere", unit:position(), 200, managers.slot:get_mask("all") )
								for foundKey,foundUnit in ipairs( foundUnits ) do
									if ( foundUnit:interaction() and foundUnit:interaction().tweak_data == "hold_take_painting" ) then
										add_waypoint(foundUnit, 'hudz_ptn_', foundKey, 'equipment_ticket')
									end
								end
							end
						end
						--ALLESO HEIST
						if ( level == 'arena' ) then
							if ( tostring(unit:name()) == "Idstring(@IDcbae338a885f1432@)") then --X mark on the wall near the mission doors
								local foundUnits = World:find_units_quick( "sphere", unit:position(), 200, managers.slot:get_mask("all") )
								for _,foundUnit in ipairs( foundUnits ) do
									if ( foundUnit:interaction() and foundUnit:interaction().tweak_data == "pick_lock_hard_no_skill_deactivated" ) then
										if (foundUnit:interaction()._active == true) then
											add_waypoint(foundUnit, 'hudz_pkgR_', key, 'wp_door')
											Waypoints.settings["showDoors"] = false --Don't show the regular doors despite what the client wants, because it would be too much information.
										end
									end
								end
							end
						end
						--MURKY STATION
						if ( level == "dark" ) then
							if ( tostring(unit:name()) == "Idstring(@IDc93d932bbb0d9d13@)" ) then 	--Blow torch
								local foundUnits = World:find_units_quick( "sphere", unit:interaction():interact_position(), 100, managers.slot:get_mask("civilians") )
								for _,civilian in ipairs( foundUnits ) do
									if (civilian:brain():is_active()) then
										if ( unit:interaction()._is_selected == nil ) then
											add_waypoint(unit, 'hudz_base_', key, 'equipment_blow_torch')
										end
									end
								end
							end
							if ( tostring(unit:name()) == "Idstring(@ID29c64eba7ea1bb4f@)" ) then 	--Thermite
								local foundUnits = World:find_units_quick( "sphere", unit:interaction():interact_position(), 100, managers.slot:get_mask("civilians") )
								for _,civilian in ipairs( foundUnits ) do
									if (civilian:brain():is_active()) then
										if ( unit:interaction()._is_selected == nil ) then
											add_waypoint(unit, 'hudz_base_', key, 'equipment_thermite')
										end
									end
								end
							end
						end
						if level == "ukrainian_job" then
							if unit:base() and tostring(unit:name()) == "Idstring(@ID077636ce1f33c8d0@)" --[[TIARA]] then
								add_waypoint(unit, 'hudz_Robj_', key, 'pd2_loot')
							end
						end
					end
					if ( level == 'shoutout_raid' ) then
						if ( tostring(unit:name()) == "Idstring(@IDaec3f706a76625a8@)" and _missionId < 5) then --Nuke boxes on Meltdown
							add_waypoint(unit, 'hudz_pkgR_', key, 'pd2_nuke')
						end
					end
					if ( unit:interaction() ~= nil and unit:interaction():active() ) then
						if _toggleWaypointAIO2 == 1 or _toggleWaypointAIO2 == 8 and Waypoints.settings["showGagePickups"] == true then
							if unit:interaction().tweak_data == 'gage_assignment' then
								if level == 'hox_2' and unit:interaction():interact_position() == Vector3(-200, -200, 4102.5) then
								else
									if tostring(unit:name()) == "Idstring(@IDe8088e3bdae0ab9e@)" then
										add_waypoint(unit, 'hudz_pkgY_', key, 'interaction_christmas_present')
									elseif tostring(unit:name()) == "Idstring(@ID05956ff396f3c58e@)" then
										add_waypoint(unit, 'hudz_pkgB_', key, 'interaction_christmas_present')
									elseif tostring(unit:name()) == "Idstring(@IDc90378ad89058c7d@)" then
										add_waypoint(unit, 'hudz_pkgP_', key, 'interaction_christmas_present')
									elseif tostring(unit:name()) == "Idstring(@ID96504ebd40f8cf98@)" then
										add_waypoint(unit, 'hudz_pkgR_', key, 'interaction_christmas_present')
									elseif tostring(unit:name()) == "Idstring(@IDb3cc2abe1734636c@)" then
										add_waypoint(unit, 'hudz_pkgG_', key, 'interaction_christmas_present')
									else
										add_waypoint(unit, 'hudz_base_', key, 'interaction_christmas_present')
									end
								end
							end
							if unit:interaction().tweak_data == 'pickup_case' then
								add_waypoint(unit, 'hudz_pkgP_', key, 'equipment_briefcase')
							end
							if unit:interaction().tweak_data == 'pickup_keys' then
								add_waypoint(unit, 'hudz_pkgY_', key, 'wp_key')
							end
						end
						if _toggleWaypointAIO2 == 2 or _toggleWaypointAIO2 == 8 then
							if ( tostring(unit:name()) == "Idstring(@ID9c0e4f7e2193a163@)" ) then --Hard drive place point
								local foundUnits = World:find_units_quick( "sphere", unit:interaction():interact_position(), 35, managers.slot:get_mask("all") )
								for _,camera in ipairs( foundUnits ) do
									if ( camera:interaction() and camera:interaction():active() == true and camera:interaction().tweak_data == 'access_camera_y_axis' )then
										if (unit:interaction():active() == true) then
											add_waypoint(unit, 'hudz_door_', key, 'equipment_harddrive')
										end
									end
								end
							end
							if level == "dark" and unit:interaction().tweak_data == 'hold_open' then
								add_waypoint(unit, 'hudz_key_', key, 'pd2_computer')
							end
							if (unit:interaction().tweak_data == 'invisible_interaction_open' or unit:interaction().tweak_data == 'take_keys') and level == 'big' then
								add_waypoint(unit, 'hudz_key_', key, 'equipment_chavez_key')
							end
							if (unit:interaction().tweak_data == 'invisible_interaction_open' or unit:interaction().tweak_data == 'rewire_electric_box') and (level == 'mus' or level == 'red2') then
								if ( unit:interaction():interact_position() ~= Vector3(6150, 549, -500) and tostring(unit:name()) ~= 'Idstring(@ID44ceafc05621d4a2@)' ) then --ATM Money
									add_waypoint(unit, 'hudz_base_', key, 'wp_powersupply')
								end
							end
							if Waypoints.settings["showDoors"] then
								if ( level ~= 'red2' ) then
									if (unit:interaction().tweak_data == 'key' or unit:interaction().tweak_data == 'key_double' or unit:interaction().tweak_data == 'hold_close_keycard' or
										unit:interaction().tweak_data == 'numpad_keycard' or unit:interaction().tweak_data == 'timelock_panel' or unit:interaction().tweak_data == 'hack_suburbia') then
										if (unit:interaction()._active == true) then
											add_waypoint(unit, 'hudz_door_', key, 'icon_locked')
										end
									end
								end
								if (unit:interaction().tweak_data == 'open_train_cargo_door' or unit:interaction().tweak_data == 'pick_lock_hard_no_skill_deactivated') then
									if (unit:interaction()._active == true) then
										add_waypoint(unit, 'hudz_door_', key, 'wp_door')
									end
								end
								if (unit:interaction().tweak_data == 'glc_open_door' ) then
									if (unit:interaction()._active == true) then
										add_waypoint(unit, 'hudz_door_', key, 'wp_door')
									end
								end
							end
							if Waypoints.settings["showCameraComputers"] then
								if ( unit:interaction().tweak_data == 'access_camera' or unit:interaction().tweak_data == 'access_camera_y_axis' and unit:interaction():active() == true ) then
									if ( level == 'arm_for' ) then
										local badWallSearch = World:find_units_quick( "sphere", unit:interaction():interact_position(), 500, managers.slot:get_mask("all") )
										local foundBadWall = false
										for foundKey,foundUnit in ipairs(badWallSearch) do
											if ( tostring(foundUnit:name()) == 'Idstring(@ID0018274d196d8432@)' and foundUnit:rotation() == Rotation(90, -0, -0) ) then
												foundBadWall = true
											end
										end
										if ( foundBadWall == false ) then
											add_waypoint(unit, 'hudz_door_', key, 'pd2_computer')
										end
									else
										add_waypoint(unit, 'hudz_door_', key, 'pd2_computer')
									end
								end
							end
						end
						if _toggleWaypointAIO2 == 3 or _toggleWaypointAIO2 == 8 then
							if unit:interaction().tweak_data == 'weapon_case' then
								if ( level == "dark" ) then
									local name = tostring(unit:name())
									if ( name == "Idstring(@ID86c151669b930ef0@)" or name == "Idstring(@ID814d28338da0dcdc@)" ) then
										add_waypoint(unit, 'hudz_wpn_', key, 'hk21')
									else
										add_waypoint(unit, 'hudz_wpn_', key, 'glock')
									end
								else
									add_waypoint(unit, 'hudz_wpn_', key, 'ak')
								end
							end
							--Cocaine
							if string.find(unit:interaction().tweak_data, 'cocaine') then
								add_waypoint(unit, 'hudz_coke_', key, 'wp_vial')
							end
						end
						if _toggleWaypointAIO2 == 4 or _toggleWaypointAIO2 == 8 then
							if ( Waypoints.settings["showPlanks"] ) then
								if unit:interaction().tweak_data == 'stash_planks_pickup' or unit:interaction().tweak_data == 'pickup_boards' then
									add_waypoint(unit, 'hudz_plk_', key, 'equipment_planks')
								end
							end
							if ( Waypoints.settings["showDrills"] ) then
								if unit:interaction().tweak_data == 'drill' then
									add_waypoint(unit, 'hudz_drill_', key, 'pd2_drill')
								elseif (unit:interaction().tweak_data == 'gen_int_saw') then
									add_waypoint(unit, 'hudz_base_', key, 'equipment_saw')
								end
							end
						end
						if _toggleWaypointAIO2 == 5 or _toggleWaypointAIO2 == 8 then
							if unit:interaction().tweak_data == 'gen_pku_crowbar' then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_crowbar')
							end
							if ( Waypoints.settings["showCrates"] ) then
								if (string.find(unit:interaction().tweak_data, 'crate_loot')) then
									add_waypoint(unit, 'hudz_base_', key, 'pd2_lootdrop')
								end
							end
						end
						if _toggleWaypointAIO2 == 6 or _toggleWaypointAIO2 == 8 then
							if level == 'hox_2' then
								if unit:interaction().tweak_data == 'firstaid_box' or unit:interaction().tweak_data == 'invisible_interaction_open' then
									add_waypoint(unit, 'hudz_base_', key, 'equipment_doctor_bag')
								end
								if unit:interaction().tweak_data == 'grenade_crate' or unit:interaction().tweak_data == 'ammo_bag' then
									add_waypoint(unit, 'hudz_base_', key, 'equipment_ammo_bag')
								end
							elseif level == 'mad' then
								if unit:interaction().tweak_data == 'gen_pku_body' then
									add_waypoint(unit, 'hudz_base_', key, 'wp_bag')
								end
							end
							if unit:interaction().tweak_data == 'hold_take_painting' then
								add_waypoint(unit, 'hudz_ptn_', key, 'equipment_ticket')
							elseif unit:interaction().tweak_data == 'use_computer' or unit:interaction().tweak_data == 'mcm_laptop' then
								add_waypoint(unit, 'hudz_base_', key, 'laptop_objective')
							elseif unit:interaction().tweak_data == 'pickup_phone' then
								add_waypoint(unit, 'hudz_base_', key, 'pd2_phone')
							elseif unit:interaction().tweak_data == 'pickup_tablet' then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_hack_ipad')
							elseif string.find(unit:interaction().tweak_data, 'server') or unit:interaction().tweak_data == 'pickup_harddrive' then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_harddrive')
							elseif unit:interaction().tweak_data == 'caustic_soda' then
								add_waypoint(unit, 'hudz_coke1_', key, 'pd2_methlab')
							elseif unit:interaction().tweak_data == 'hydrogen_chloride' or unit:interaction().tweak_data == 'hold_turn_off_gas' then
								add_waypoint(unit, 'hudz_coke2_', key, 'pd2_methlab')
							elseif unit:interaction().tweak_data == 'muriatic_acid' then
								add_waypoint(unit, 'hudz_coke3_', key, 'pd2_methlab')
							elseif unit:interaction().tweak_data == 'hold_pku_drk_bomb_part' then
								add_waypoint(unit, 'hudz_base_', key, 'pd2_nuke')
							elseif unit:interaction().tweak_data == 'hold_pku_briefcase' then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_briefcase')
							elseif unit:interaction().tweak_data == 'hold_remove_hand' then
								add_waypoint(unit, 'hudz_cop_', key, 'equipment_hand')
							elseif (unit:interaction().tweak_data == 'press_printer_ink') then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_printer_ink')
							elseif (unit:interaction().tweak_data == 'press_printer_paper') then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_paper_roll')
							elseif unit:interaction().tweak_data == 'hold_grab_goat' then
								add_waypoint(unit, 'hudz_cashB_', key, 'equipment_briefcase')
							elseif unit:interaction().tweak_data == 'driving_drive' then
								add_waypoint(unit, 'hudz_car_', key, unit:interaction()._ray_object_names[6] ~= nil and 'pd2_car' or 'equipment_ejection_seat')
							elseif unit:interaction().tweak_data == 'mcm_fbi_taperecorder' then
								add_waypoint(unit, 'hudz_base_', key, 'pd2_talk')
							elseif unit:interaction().tweak_data == 'gen_pku_evidence_bag' or unit:interaction().tweak_data == 'invisible_interaction_gathering' then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_evidence')
							elseif unit:interaction().tweak_data == 'hospital_security_cable' then
								add_waypoint(unit, 'hudz_Robj_', key, 'pd2_wirecutter')
							elseif unit:interaction().tweak_data == 'mcm_break_planks' then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_planks')
							elseif unit:interaction().tweak_data == 'mcm_panicroom_keycard_1' then
								add_waypoint(unit, 'hudz_door_', key, 'wp_powerbutton')
							elseif unit:interaction().tweak_data == 'mcm_panicroom_keycard_2' then
								add_waypoint(unit, 'hudz_door_', key, 'wp_powerbutton')
							elseif unit:interaction().tweak_data == 'take_chainsaw' then
								add_waypoint(unit, 'hudz_base_', key, 'pd2_chainsaw')
							elseif unit:interaction().tweak_data == 'use_chainsaw' then
								add_waypoint(unit, 'hudz_base_', key, 'pd2_chainsaw')
							elseif unit:interaction().tweak_data == 'hold_remove_ladder' then
								add_waypoint(unit, 'hudz_base_', key, 'pd2_ladder')
							elseif unit:interaction().tweak_data == 'hold_open_xmas_present' or unit:interaction().tweak_data == 'hold_take_vr_headset' then
								add_waypoint(unit, 'hudz_base_', key, 'interaction_christmas_present')
							elseif unit:interaction().tweak_data == 'hold_open_shopping_bag' then
								add_waypoint(unit, 'hudz_base_', key, 'wp_bag')
							elseif unit:interaction().tweak_data == 'hold_take_mask' then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_chrome_mask')
							elseif unit:interaction().tweak_data == 'gen_pku_sandwich' then
								add_waypoint(unit, 'hudz_base_', key, 'pd2_loot')
							elseif unit:interaction().tweak_data == 'invisible_interaction_searching' then
								add_waypoint(unit, 'hudz_Robj_', key, 'equipment_files')
							elseif unit:interaction().tweak_data == 'hold_download_keys' or unit:interaction().tweak_data == 'security_station_keyboard' then
								add_waypoint(unit, 'hudz_base_', key, 'interaction_keyboard')
							elseif string.find(unit:interaction().tweak_data, 'circuit') or unit:interaction().tweak_data == 'hold_remove_cover' or 
								   unit:interaction().tweak_data == 'hold_cut_cable' then
								add_waypoint(unit, 'hudz_base_', key, 'wp_powersupply')
							elseif unit:interaction().tweak_data == 'c4_bag' then
								add_waypoint(unit, 'hudz_key_', key, 'equipment_c4')
							elseif unit:interaction().tweak_data == 'c4_mission_door' then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_c4')
							elseif unit:interaction().tweak_data == 'hold_take_gas_can' then
								add_waypoint(unit, 'hudz_fire_', key, 'equipment_thermite')
							elseif unit:interaction().tweak_data == 'disassemble_turret' then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_sentry')
							elseif unit:interaction().tweak_data == 'take_confidential_folder_event' or unit:interaction().tweak_data == 'take_confidential_folder' or 
								   unit:interaction().tweak_data == 'hold_take_blueprints' or unit:interaction().tweak_data == 'pickup_asset' then
								add_waypoint(unit, 'hudz_base_', key, 'interaction_patientfile')
							elseif unit:interaction().tweak_data == 'sewer_manhole' and Waypoints.settings["showSewerManhole"] then
								add_waypoint(unit, 'hudz_door_', key, 'interaction_open_door')
							elseif unit:interaction().tweak_data == 'apply_thermite_paste' and Waypoints.settings["showThermite"] then
								add_waypoint(unit, 'hudz_fire_', key, 'equipment_thermite')
							elseif unit:interaction().tweak_data == 'hold_blow_torch' then
								add_waypoint(unit, 'hudz_fire_', key, 'equipment_blow_torch')
							elseif unit:interaction().tweak_data == 'votingmachine2' then
								add_waypoint(unit, 'hudz_base_', key, 'pd2_computer')
							elseif string.find(unit:interaction().tweak_data, 'gen_pku_artifact') or unit:interaction().tweak_data == 'samurai_armor' then
								if unit:interaction().tweak_data == 'gen_pku_artifact_painting' then
									if level ~= "mus" or (unit:unit_data() and unit:unit_data().unit_id ~= 300047) then --[[Painting that can't be grabbed from The Diamond]]
										add_waypoint(unit, 'hudz_ptn_', key, 'equipment_ticket')
									end
								else					
									add_waypoint(unit, 'hudz_cashB_', key, 'wp_scrubs')
								end
							elseif unit:interaction().tweak_data == 'uload_database' then
								add_waypoint(unit, 'hudz_base_', key, 'pd2_computer')
							elseif unit:interaction().tweak_data == 'pku_pig' then
								add_waypoint(unit, 'hudz_cashB_', key, 'equipment_briefcase')
							end
						end
						if _toggleWaypointAIO2 == 7 or _toggleWaypointAIO2 == 8 then
							-- Money
							if unit:interaction().tweak_data == 'money_wrap_updating' then
								add_waypoint(unit, 'hudz_cashB_', key, 'equipment_money_bag')
							end
							-- money
							if unit:interaction().tweak_data == 'money_wrap_single_bundle' and Waypoints.settings["showSmallLoot"] then
								add_waypoint(unit, 'hudz_cash_', key, 'interaction_money_wrap')
							elseif unit:interaction().tweak_data == 'cash_register' and Waypoints.settings["showSmallLoot"] then
								if level == "jewelry_store" or level == "ukrainian_job" then
									if unit:position() == Vector3(1844, 665, 117.732) then
									else
										add_waypoint(unit, 'hudz_cash_', key, 'interaction_money_wrap')
									end
								else
									add_waypoint(unit, 'hudz_cash_', key, 'interaction_money_wrap')
								end
							elseif unit:interaction().tweak_data == 'money_small' then
								add_waypoint(unit, 'hudz_cashB_', key, 'interaction_money_wrap')
							elseif unit:interaction().tweak_data == 'money_wrap' then
								if level == "welcome_to_the_jungle_1" then
									if unit:position() ~= Vector3(9200, -4300, 100) then
										add_waypoint(unit, 'hudz_cashB_', key, 'equipment_money_bag')
									end
								elseif level == "family" then
									if unit:position() ~= Vector3(1400, 200, 1100) then
										add_waypoint(unit, 'hudz_cashB_', key, 'equipment_money_bag')
									end
								elseif level == "mia_1" then
									if unit:position() ~= Vector3(5400, 1400, -300) then
										add_waypoint(unit, 'hudz_cashB_', key, 'equipment_money_bag')
									end
								else
									add_waypoint(unit, 'hudz_cashB_', key, 'equipment_money_bag')
								end
							-- gold
							elseif unit:interaction().tweak_data == 'gold_pile' then
								if level == "welcome_to_the_jungle_1" then
									if unit:position() ~= Vector3(9200, -4400, 100) then
										add_waypoint(unit, 'hudz_gold_', key, 'interaction_gold')
									end
								else
									add_waypoint(unit, 'hudz_gold_', key, 'interaction_gold')
								end
							-- diamonds/jewels
							elseif unit:interaction().tweak_data == 'diamond_pickup' and Waypoints.settings["showSmallLoot"] then
								add_waypoint(unit, 'hudz_gold_', key, 'interaction_diamond')
							elseif unit:interaction().tweak_data == 'gen_pku_jewelry' then
								add_waypoint(unit, 'hudz_cashB_', key, 'wp_bag')
							-- ATMS
							elseif unit:interaction().tweak_data == 'requires_ecm_jammer_atm' then
								add_waypoint(unit, 'hudz_atm_', key, 'equipment_ecm_jammer')
							-- safe loot
							elseif unit:interaction().tweak_data == 'safe_loot_pickup' and Waypoints.settings["showSmallLoot"] then
								if level == "family" then
									if unit:position().z < 1000 then	-- Vector3(1400, 100, 1100)
										add_waypoint(unit, 'hudz_cash_', key, 'interaction_money_wrap')
									end
								else
									add_waypoint(unit, 'hudz_cash_', key, 'interaction_money_wrap')
								end
							end
							-- train heist ammo
							if unit:interaction().tweak_data == 'take_ammo' and level == "arm_for" then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_ammo_bag')
							end
							--Toothbrush
							if unit:interaction().tweak_data == 'pku_toothbrush' then
								add_waypoint(unit, 'hudz_pkgR_', key, 'pd2_water_tap')
							end
							--Animal Poster
							if unit:interaction().tweak_data == 'hold_take_missing_animal_poster' then
								add_waypoint(unit, 'hudz_coke1_', key, 'equipment_ticket')
							end
							--Turtle
							if unit:interaction().tweak_data == 'hold_pick_up_turtle' then
								add_waypoint(unit, 'hudz_pkgG_', key, 'equipment_briefcase')
							end
							--Handcuffs
							if unit:interaction().tweak_data == 'glc_hold_take_handcuffs' then
								add_waypoint(unit, 'hudz_base_', key, 'equipment_handcuffs')
							end
						end
					end
				end
				RefreshMsg()
			end
		end
	end

	-- initialize waypoints
	RefreshItemWaypoints(true)
	
	--This function is called when an item is removed from the session.
	managers.interaction._remove_unit = managers.interaction._remove_unit or managers.interaction.remove_unit
	function ObjectInteractionManager:remove_unit( unit )
		local interacted = unit:interaction().tweak_data
		local result = self:_remove_unit(unit)
		local position = unit:position()
		local position2 = unit:interaction():interact_position()
		local level = managers.job:current_level_id()
		
		if game_state_machine:current_state_name() == "victoryscreen" or string.find(game_state_machine:current_state_name(), "victoryscreen")
		or game_state_machine:current_state_name() == "gameoverscreen" or string.find(game_state_machine:current_state_name(), "gameoverscreen") then
			_toggleWaypointAIO2 = 0
			if ( Waypoints.settings["showHUDMessages"] ) then
				if _HDmsg then
					_HDmsg.lbl:set_text("")
					_HDmsg.lbl2:set_text("")
					_HDmsg.lbl3:set_text("")
					_HDmsg.lbl4:set_text("")
				end
			end
			return result
		end
		
		if ( unit:interaction() and unit:interaction()._waypoint_id ~= nil ) then
			remove_waypoint(unit)
		end

		return result
	end
	
	managers.interaction._add_unit = managers.interaction._add_unit or managers.interaction.add_unit
	function ObjectInteractionManager:add_unit( unit )
		local spawned = unit:interaction().tweak_data
		local result = self:_add_unit(unit)
		local level = managers.job:current_level_id()
		
		if ( spawned == "hostage_move" or spawned == "intimidate" ) then
			return result
		end
		
		if spawned == "hlm_roll_carpet" then
			managers.hud:remove_waypoint( "hudz_base_hlm1door" )
			return result
		end
		
		-- KEYCARD COP/CIV : REMOVE WAYPOINT OF THE UNIT WHEN KEYCARD IS DROPPED
		if spawned == 'pickup_keycard' or spawned == 'hold_pku_knife' then
			find_associated_npc_waypoint(unit)
		end
		
		RefreshItemWaypoints(false)
		return result
	end
end