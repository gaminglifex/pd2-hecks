--[[ * Give All Challenges v2 by gir489
	 *
	 * Changelog: 
	 *		v1: Initial release
	 *		v2: Added safehouse challenge completion call.
	 *
	 *	Not for use in Pirate Perfagtion Trainer]]
local doYouLikeHurtingPeople = doYouLikeHurtingPeople or ChallengeManager.activate_challenge
function ChallengeManager:activate_challenge(id, key, category)
	managers.custom_safehouse:complete_daily()
	if self:has_active_challenges(id, key) then
		local active_challenge = self:get_active_challenge(id, key)
		active_challenge.completed = true
		active_challenge.category = category
		return true
	end
	return doYouLikeHurtingPeople(self, id, key, category)
end