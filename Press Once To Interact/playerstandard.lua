PlayerStandard.EQUIPMENT_PRESS_INTERRUPT = true --Use equipment key "G" to stop interacting (default false)
local MIN_TIMER_DURATION = 0 --Interaction duration (in seconds) for the toggle behavior to activate (default 0)

local PlayerStandard__check_action_interact_original = PlayerStandard._check_action_interact
function PlayerStandard:_check_action_interact(t, input)
	local interrupt_key_press = input.btn_interact_press
		if PlayerStandard.EQUIPMENT_PRESS_INTERRUPT then
			interrupt_key_press = input.btn_use_item_press
		end
	if interrupt_key_press and self:_interacting() then
		self:_interupt_action_interact()
		return false
	elseif input.btn_interact_release and self._interact_params then
		if self._interact_params.timer >= MIN_TIMER_DURATION then
			return false
		end
	end

	return PlayerStandard__check_action_interact_original(self, t, input)
end