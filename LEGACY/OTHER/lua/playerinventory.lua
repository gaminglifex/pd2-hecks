PlayerInventory._add_unit_by_factory_name = PlayerInventory._add_unit_by_factory_name or PlayerInventory.add_unit_by_factory_name
function PlayerInventory:add_unit_by_factory_name(factory_name, equip, instant, blueprint, texture_switches, ...)
	local factory_weapon = tweak_data.weapon.factory[factory_name]
	local ids_unit_name = Idstring(factory_weapon.unit)
	managers.dyn_resource:load(Idstring("unit"), ids_unit_name, "packages/dyn_resources", false)
	return self:_add_unit_by_factory_name(factory_name, equip, instant, blueprint, texture_switches, ...)
end