local weaponFactorySetup = WeaponFactoryManager._setup
function WeaponFactoryManager:_setup()
	weaponFactorySetup(self)
	tweak_data.weapon.ak74.fire_mode_data.fire_rate = 0.15
	tweak_data.weapon.ak74.auto.fire_rate = 0.15
	tweak_data.weapon.akmsu.auto.fire_rate = 0.17
	tweak_data.weapon.akmsu.fire_mode_data.fire_rate = 0.17
	tweak_data.weapon.m249.fire_mode_data.fire_rate = 0.08
	tweak_data.weapon.m134.fire_mode_data.fire_rate = 0.04
	tweak_data.weapon.par.fire_mode_data.fire_rate = 0.08

	--tweak_data.blackmarket.melee_weapons.shovel.stats.min_damage = 1
	tweak_data.blackmarket.melee_weapons.shovel.stats.max_damage = 69
	tweak_data.blackmarket.melee_weapons.shovel.stats.min_damage_effect = 69.69
	tweak_data.blackmarket.melee_weapons.shovel.stats.max_damage_effect = 69.69
	tweak_data.blackmarket.melee_weapons.shovel.stats.charge_time = .69

	--[[* All Parts Have Max Stats by gir489 v1
		*
		* Changelog:
		*	v1: Initial Release
		*
		* Not for use in Pirate Perfagtion Trainer]]
	for _,part in pairs( tweak_data.weapon.factory.parts ) do
		if ( part.stats ) then
			if ( part.stats.damage ) then
				if ( part.stats.damage < 11) then
					part.stats.damage = 11
				end
			end
			if ( part.stats.recoil ) then
				if ( part.stats.recoil < 0 ) then
					part.stats.recoil = 0
				end
			end
			if ( part.stats.spread ) then
				if ( part.stats.spread < 0 ) then
					part.stats.spread = 0
				end
			end
			if ( part.stats.total_ammo_mod ) then
				if ( part.stats.total_ammo_mod < 0 ) then
					part.stats.total_ammo_mod = 0
				end
			end
			part.stats.suppression = -20
		end
	end
	
	tweak_data.weapon.factory.parts.wpn_fps_lmg_par_b_short.stats.damage = 30
end

if not _hasPerk then _hasPerk = WeaponFactoryManager.has_perk end
function WeaponFactoryManager:has_perk( perk_name, factory_id, blueprint )
	if ( perk_name == "silencer" ) then
		return true
	end
	return _hasPerk(self, perk_name, factory_id, blueprint)
end
