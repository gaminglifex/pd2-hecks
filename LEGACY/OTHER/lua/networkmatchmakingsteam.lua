--[[ * Doing OVERKILL's Job Part 2 by gir489 v2
	 *
	 * Changelog: 
	 *		v1: Initial release
	 *		v2: Added value clamping.
	 *
	 *	Not for use in Pirate Perfagtion Trainer]]
if not _setAtribsSteamMatchMaking then _setAtribsSteamMatchMaking = NetworkMatchMakingSTEAM.set_attributes end 
function NetworkMatchMakingSTEAM:set_attributes(settings)
	_setAtribsSteamMatchMaking(self, settings)
	if ( self._lobby_attributes and self.lobby_handler ) then
		if ( self._lobby_attributes.job_class_min > 100 ) then
			self._lobby_attributes.job_class_min = 100
		end
		if ( self._lobby_attributes.job_class_max > 100 ) then
			self._lobby_attributes.job_class_max = 100
		end
		self.lobby_handler:set_lobby_data(self._lobby_attributes)
	end
end

function NetworkMatchMakingSTEAM:set_num_players(num)
	print("NetworkMatchMakingSTEAM:set_num_players", num)
	self._num_players = num
	if self._lobby_attributes and self.lobby_handler then
		self._lobby_attributes.num_players = num
		self.lobby_handler:set_lobby_data(self._lobby_attributes)
	end
end