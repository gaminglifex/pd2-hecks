if not _musicManagerJukeBoxMenuTrack then _musicManagerJukeBoxMenuTrack = MusicManager.jukebox_menu_track end
function MusicManager:jukebox_menu_track(name)
	if ( name == "loadout" ) then
		local loadout_tracks = {
			"preplanning_music",
			"preplanning_music_old",
			"pth_preparations"
		}
		return loadout_tracks[math.random(#loadout_tracks)]
	else
		return _musicManagerJukeBoxMenuTrack(self, name)
	end
end