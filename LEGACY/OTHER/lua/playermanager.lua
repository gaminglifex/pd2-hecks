function isLMG()
	if ( managers.player:player_unit():inventory():equipped_unit() == nil ) then
		return false
	end
	local equipped_weapon = managers.player:player_unit():inventory():equipped_unit():base():weapon_tweak_data()
	if equipped_weapon then
		if equipped_weapon.category == "lmg" then 
			return true
		end
	end
    return false
end

--Fix body armor sucking.
if not _bodyArmor then _bodyArmor = PlayerManager.body_armor_value end
function PlayerManager:body_armor_value( category, override_value, default )
	if ( category == "dodge" ) then
		local dodge = _bodyArmor(self, category, override_value, default )
		if ( dodge <= -0.1 ) then
			return -0.1
		else
			return dodge
		end
	elseif ( category == "stamina" ) then
		return 1.025
	end
	
	return _bodyArmor(self, category, override_value, default )
end

--Allows you to pick up bags right after you throw them.
function PlayerManager:carry_blocked_by_cooldown() 
	return false
end

_originalUpgrade = _originalUpgrade or PlayerManager.upgrade_value
function PlayerManager:upgrade_value( category, upgrade, default )
	if alive( managers.player:local_player() ) and not managers.trade:is_peer_in_custody( managers.network:session():local_peer():id() ) then
		if isLMG() then
			local player = managers.player:local_player()
			if ( player:movement() and player:movement():current_state() and player:movement():current_state():in_steelsight() == false ) then
				managers.hud:on_crit_confirmed()
			end
		end
		--[[local civilians = managers.enemy:all_civilians()
		for _,civilian in pairs( civilians ) do
			if ( alive(civilian.unit) ) then
				if ( civilian.unit:anim_data() ) then
					civilian.unit:set_slot( civilian.unit:anim_data().tied and 22 or 21 )
				end
			end
		end]]
	end
	if category == "weapon" and upgrade == "armor_piercing_chance" then
		return .8
	elseif category == "weapon" and upgrade == "armor_piercing_chance_silencer" then
		return .8
	elseif category == "weapon" and upgrade == "passive_headshot_damage_multiplier" then
		return 4
	elseif category == "player" and upgrade == "passive_dodge_chance" then
		return .25
	elseif category == "player" and upgrade == "passive_damage_multiplier" then
		return 1.3
	elseif category == "player" and upgrade == "run_dodge_chance" then
		return .5
	elseif category == "player" and upgrade == "crouch_dodge_chance" then
		return .25
	elseif category == "player" and upgrade == "armor_multiplier" then
		return 2
	elseif category == "player" and upgrade == "convert_enemies_max_minions" then
		return 2
	else
		return _originalUpgrade(self, category, upgrade, default)
	end
end

--Fix for infinite C4 usage.
if not _removeEquip then _removeEquip = PlayerManager.remove_equipment end 
function PlayerManager:remove_equipment( equipment_id )
	local equipment = self:equipment_data_by_name( equipment_id )
	if ( equipment ) then
		if ( Application:digest_value( equipment.amount, false ) > 0 ) then
			_removeEquip(self, equipment_id )
		end
	end
end

_useBodyBag = _useBodyBag or PlayerManager.on_used_body_bag
function PlayerManager:on_used_body_bag()
	if( self._local_player_body_bags > 0 ) then
		_useBodyBag(self)
	end
end

function PlayerManager:has_special_equipment( name )
	if Input:keyboard():down( Idstring("left alt") ) then
		return true
	end
	return self._equipment.specials[ name ]
end