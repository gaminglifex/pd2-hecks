if managers.hud then
	if Input:keyboard():down( Idstring("left shift") ) and Network:is_server() == true then
		-- Allow restart on pro jobs 
		if not _oldIsProfessional then _oldIsProfessional = JobManager.is_current_job_professional end 
		if not _newIsProfessional then _newIsProfessional = function(self) return false end end 
		if not _lobbyStateEnter then _lobbyStateEnter = IngameLobbyMenuState.at_enter end 
		function IngameLobbyMenuState:at_enter() 
		   JobManager.is_current_job_professional = _newIsProfessional 
		   _lobbyStateEnter(self) 
		   JobManager.is_current_job_professional = _oldIsProfessional 
		end 
		if not _lobbyContinue then _lobbyContinue = GameOverState.continue end 
		function GameOverState:continue() 
		   JobManager.is_current_job_professional = _newIsProfessional 
		   _lobbyContinue(self) 
		   JobManager.is_current_job_professional = _oldIsProfessional 
		end 
		if not _lobbyShutDownNetwork then _lobbyShutDownNetwork = GameOverState._shut_down_network end 
		function GameOverState:_shut_down_network( ... ) 
		   JobManager.is_current_job_professional = _newIsProfessional 
		   _lobbyShutDownNetwork(self, ...) 
		   JobManager.is_current_job_professional = _oldIsProfessional 
		end 
		if not _lobbyLoadStartMenu then _lobbyLoadStartMenu = GameOverState._load_start_menu end 
		function GameOverState:_load_start_menu( ... ) 
		   JobManager.is_current_job_professional = _newIsProfessional 
		   _lobbyLoadStartMenu(self, ...) 
		   JobManager.is_current_job_professional = _oldIsProfessional 
		end 
		if not _lobbySetBtnText then _lobbySetBtnText = GameOverState._set_continue_button_text end 
		function GameOverState:_set_continue_button_text() 
		   JobManager.is_current_job_professional = _newIsProfessional 
		   _lobbySetBtnText(self) 
		   JobManager.is_current_job_professional = _oldIsProfessional 
		end 
		function MenuCallbackHandler:singleplayer_restart() return true end
		if managers.platform:presence() == "Playing" then 
			managers.network:session():send_to_peers( "mission_ended", false, 0 ) 
			game_state_machine:change_state_by_name( "gameoverscreen" )
		end
	end
end