if managers.player:player_unit() then
	function secure( name )
		local multiplier = managers.player:upgrade_value_by_level( "player", "small_loot_multiplier", managers.player:upgrade_level( "player", "small_loot_multiplier" ), 1 )
		managers.loot:show_small_loot_taken_hint(name, multiplier)
		managers.loot:secure_small_loot( name, multiplier )
	end
	secure("gen_atm")
else
	function add_job(job_id, difficulty) 
	   local difficulty_id = tweak_data:difficulty_to_index( difficulty ) 
	   table.insert( managers.crimenet._presets, { job_id = job_id, difficulty_id = difficulty_id, difficulty = difficulty, chance = 1 } ) 
	   managers.crimenet._MAX_ACTIVE_JOBS = managers.crimenet._MAX_ACTIVE_JOBS + 1 
	end 
	-- _NEW_JOB_MIN_TIME/_NEW_JOB_MAX_TIME are a better alternative to managers.crimenet._debug_mass_spawning 
	managers.crimenet._NEW_JOB_MIN_TIME = 0 
	managers.crimenet._NEW_JOB_MAX_TIME = 0 
	managers.crimenet._presets = { } 
	managers.crimenet._active_jobs = { } 
	managers.crimenet._MAX_ACTIVE_JOBS = 0 
	add_job("welcome_to_the_jungle_prof", "overkill_290")
	add_job("watchdogs_prof", "overkill_290")
	add_job("firestarter_prof", "overkill_290")
	add_job("framing_frame_prof", "overkill_290")
	add_job("alex_prof", "overkill_290")
	add_job("nightclub", "overkill_290")
	add_job("branchbank_gold_prof", "overkill_290")
	add_job("branchbank_cash", "overkill_290")
	--add_job("branchbank_cash_prof", "overkill_290") This crashes for some reason.
	add_job("election_day", "overkill_290")
	add_job("election_day_prof", "overkill_290")
	add_job("hox_prof", "overkill_290")
end