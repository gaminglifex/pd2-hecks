function GatoraidMeBitch(name)
	local special_equip = managers.player._equipment.specials[name]
	if special_equip == nil then
		return true
	end
	local deigested_amount = special_equip.amount and Application:digest_value(special_equip.amount, false) or 1
	return deigested_amount < 4
end

if not only98PercentPure then only98PercentPure = BaseInteractionExt.can_select end
function BaseInteractionExt:can_select(player)
	if (self._tweak_data.special_equipment_block == "acid" or self._tweak_data.special_equipment_block == "hydrogen_chloride" or self._tweak_data.special_equipment_block == "caustic_soda") then
		if not self:_has_required_upgrade(alive(player) and player:movement() and player:movement().current_state_name and player:movement():current_state_name()) then
			return false
		end
		if not self:_has_required_deployable() then
			return false
		end
		if not self:_is_in_required_state(alive(player) and player:movement() and player:movement().current_state_name and player:movement():current_state_name()) then
			return false
		end
		return GatoraidMeBitch(self._tweak_data.special_equipment_block)
	end
	return only98PercentPure(self, player)
end

if not HCLPlusCSPlusMUEqualsSaltWater then HCLPlusCSPlusMUEqualsSaltWater = BaseInteractionExt.can_interact end
function BaseInteractionExt:can_interact(player)
	if (self._tweak_data.special_equipment_block == "acid" or self._tweak_data.special_equipment_block == "hydrogen_chloride" or self._tweak_data.special_equipment_block == "caustic_soda") then
		if (managers.player:current_state() == "clean" or managers.player:current_state() == "mask_off") and not self._tweak_data.can_interact_in_casing then
			return false
		end
		if not self:_has_required_upgrade(alive(player) and player:movement() and player:movement().current_state_name and player:movement():current_state_name()) then
			return false
		end
		if not self:_has_required_deployable() then
			return false
		end
		return GatoraidMeBitch(self._tweak_data.special_equipment_block)
	end
	return HCLPlusCSPlusMUEqualsSaltWater(self, player)
end