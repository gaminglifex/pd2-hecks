local iamtheonewhoknocksBITCH = iamtheonewhoknocksBITCH or EquipmentsTweakData.init
function EquipmentsTweakData:init()
	iamtheonewhoknocksBITCH(self)
	self.specials.acid.max_quantity = 4
	self.specials.caustic_soda.max_quantity = 4
	self.specials.hydrogen_chloride.max_quantity = 4
end