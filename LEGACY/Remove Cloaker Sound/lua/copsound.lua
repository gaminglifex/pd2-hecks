_CopSound_play = _CopSound_play or CopSound.play
function CopSound:play( sound_name, source_name, sync )
	local event_id
	if type( sound_name ) == "number" then
	event_id = sound_name
	else
		event_id = SoundDevice:string_to_id( sound_name )
	end
	
	if ( sound_name == "cloaker_detect_mono" ) then
		self._unit:contour():add( "mark_enemy", false, 9999 )
		if alive( managers.player:local_player() ) and not managers.trade:is_peer_in_custody( managers.network:session():local_peer():id() ) then
			managers.player:local_player():sound():say( "f33x_any", false, false )	
		end
		managers.hud:show_hint( { text = "CLOAKER ATTACKING" } )
		return nil
	elseif ( sound_name == "cloaker_detect_stop") then
		return nil
	elseif ( sound_name == "cloaker_presence_loop" ) then
		local _dieBase = CopDamage.die
		function CopDamage:die( variant )
			if self._unit:contour() then
					self._unit:contour():remove("mark_enemy", false)
			end
			_dieBase(self, variant)
		end
		local _huskDieBase = HuskCopDamage.die
		function HuskCopDamage:die( variant )
			if self._unit:contour() then
				self._unit:contour():remove("mark_enemy", false)
			end
			_huskDieBase(self, variant)
		end
		self._unit:contour():add( "mark_enemy", false, 9999 )
		if alive( managers.player:local_player() ) and not managers.trade:is_peer_in_custody( managers.network:session():local_peer():id() ) then
			managers.player:local_player():sound():say( "f33x_any", false, false )
		end
		return nil
	else
		return _CopSound_play( self, sound_name, source_name, sync )
	end
end