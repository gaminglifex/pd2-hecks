{
    "name" : "Kill Counter Standalone",
    "description" : "Counts the kills and displays them on your hud.",
    "author" : "Seven"
    "contact" : "",
    "version" : "2.00",
    "priority" : 12,
    "hooks" : [   
        {"hook_id" : "lib/units/equipment/sentry_gun/sentrygunbase", "script_path" : "killcounter.lua" },   
        {"hook_id" : "lib/units/enemies/cop/copdamage", "script_path" : "killcounter.lua" },
        {"hook_id" : "lib/managers/hudmanagerpd2", "script_path" : "killcounter.lua" },
        {"hook_id" : "lib/managers/hud/hudteammate", "script_path" : "killcounter.lua" }   
	]
	
}
