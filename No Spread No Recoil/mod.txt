{
	"name": "No Spread/No Recoil",
	"description": "Removes the spread and recoil from most weapons.",
	"author": "gir489",
	"contact": "https://bitbucket.org/gir489/",
	"version": "1",
	"priority": 1000,
	"hooks" : [
		{ "hook_id" : "lib/units/weapons/newraycastweaponbase", "script_path" : "newraycastweaponbase.lua" }
	]
}