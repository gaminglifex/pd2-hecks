--[[ * Give all Perk Decks v2
	 *
	 * Changelog: 
	 *		v1: Initial release
	 *		v2: Blacklisted bad perks
	 *
	 *	Not for use in Pirate Perfagtion Trainer]]
if not _atEnter then _atEnter = MenuTitlescreenState.at_enter end
function MenuTitlescreenState:at_enter()
	_atEnter(self)
	for _,specialization in ipairs(tweak_data.skilltree.specializations) do
		for _,tree in ipairs(specialization) do
			if ( tree.upgrades ) then
				for _,upgrade in ipairs(tree.upgrades) do
					if ( (not upgrade:find("loss")) and (not upgrade:find("penalty")) --[[and (not upgrade:find("reduction"))]] ) then
						managers.upgrades:aquire(upgrade,false)
					end
				end
			end
		end
	end
end