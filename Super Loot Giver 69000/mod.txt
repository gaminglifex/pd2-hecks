{
	"name": "Super Loot Giver 69000",
	"description": "Gives you loot, see?",
	"author": "gir489",
	"contact": "https://bitbucket.org/gir489/",
	"version": "1",
	"priority": 500,
	"keybinds": [
		{
			"keybind_id": "GiveLootHotkey",
			"name": "SuperLootGiver69000",
			"description": "Key to open the loot menu",
			"script_path": "loot.lua",
			"run_in_menu": false,
			"run_in_game": true,
			"localized": false
		}
	]
}