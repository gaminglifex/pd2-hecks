--[[ * Super Loot Giver 69000 by gir489 v4
	 *
	 * Credits; Harfatus for SimpleMenu.
	 *
	 * Changelog: 
	 *		v1: Initial release
	 *		v2: Added close option to menu
	 *		v3: Fixed the game crashing on certain packages.
	 *		v4: Separated the loot in to categories.
	 *			Added option to give more than 1 bag.
	 *
	 *	Not for use in Pirate Perfagtion Trainer]]
if not SimpleMenu then
    SimpleMenu = class()

    function SimpleMenu:init(title, message, options)
        self.dialog_data = { title = title, text = message, button_list = {},
                             id = tostring(math.random(0,0xFFFFFFFF)) }
        self.visible = false
        for _,opt in ipairs(options) do
            local elem = {}
            elem.text = opt.text
            opt.data = opt.data or nil
            opt.callback = opt.callback or nil
            elem.callback_func = callback(self, self, "_do_callback",
                                          { data = opt.data,
                                            callback = opt.callback})
            elem.cancel_button = opt.is_cancel_button or false
            if opt.is_focused_button then
                self.dialog_data.focus_button = #self.dialog_data.button_list+1
            end
            table.insert(self.dialog_data.button_list, elem)
        end
        return self
    end

    function SimpleMenu:_do_callback(info)
        if info.callback then
            if info.data then
                info.callback(info.data)
            else
                info.callback()
            end
        end
        self.visible = false
    end

    function SimpleMenu:show()
        if self.visible then
            return
        end
        self.visible = true
        managers.system_menu:show(self.dialog_data)
    end

    function SimpleMenu:hide()
        if self.visible then
            managers.system_menu:close(self.dialog_data.id)
            self.visible = false
            return
        end
    end
end

patched_update_input = patched_update_input or function (self, t, dt )
    if self._data.no_buttons then
        return
    end
    
    local dir, move_time
    local move = self._controller:get_input_axis( "menu_move" )

    if( self._controller:get_input_bool( "menu_down" )) then
        dir = 1
    elseif( self._controller:get_input_bool( "menu_up" )) then
        dir = -1
    end
    
    if dir == nil then
        if move.y > self.MOVE_AXIS_LIMIT then
            dir = 1
        elseif move.y < -self.MOVE_AXIS_LIMIT then
            dir = -1
        end
    end

    if dir ~= nil then
        if( ( self._move_button_dir == dir ) and self._move_button_time and ( t < self._move_button_time + self.MOVE_AXIS_DELAY ) ) then
            move_time = self._move_button_time or t
        else
            self._panel_script:change_focus_button( dir )
            move_time = t
        end
    end

    self._move_button_dir = dir
    self._move_button_time = move_time
    
    local scroll = self._controller:get_input_axis( "menu_scroll" )
    -- local sdir
    if( scroll.y > self.MOVE_AXIS_LIMIT ) then
        self._panel_script:scroll_up()
        -- sdir = 1
    elseif( scroll.y < -self.MOVE_AXIS_LIMIT ) then
        self._panel_script:scroll_down()
        -- sdir = -1
    end
end
managers.system_menu.DIALOG_CLASS.update_input = patched_update_input
managers.system_menu.GENERIC_DIALOG_CLASS.update_input = patched_update_input

function givebag(info)
	opts = {}
	table.insert( opts, { text = "1", callback = giveloot, data = { name = info, amount = 1 }, is_focused_button = true } )
	table.insert( opts, { text = "5", callback = giveloot, data = { name = info, amount = 5 } } )
	table.insert( opts, { text = "10", callback = giveloot, data = { name = info, amount = 10 } } )
	table.insert( opts, { text = "15", callback = giveloot, data = { name = info, amount = 15 } } )
	table.insert( opts, { text = "25", callback = giveloot, data = { name = info, amount = 25 } } )
 	table.insert( opts, { text = "50", callback = giveloot, data = { name = info, amount = 50 } } )
	table.insert( opts, { text = "100", callback = giveloot, data = { name = info, amount = 100 } } )
	table.insert( opts, { text = "Close", is_cancel_button = true } )
	mymenu = SimpleMenu:new("How many", "", opts)
	mymenu:show()
end

function giveloot(info)
	if ( managers.player:player_unit() ) then
		while ( info.amount > 0 ) do
			managers.loot:secure( info.name, managers.money:get_bag_value( info.name ), true ) --When you are the host, other people won't know that you secured these extra bags. When not the host, everyone will see it, but they won't know who's doing it. Always blame the host. Then crash the server for extra super kek supremes.
			info.amount = info.amount - 1
		end
	end
end

function doGeneric()
	opts = {}
	for i, carry in pairs(tweak_data.carry) do
		if ( carry ) then
			if carry.name_id and carry.bag_value then
				table.insert( opts, { text = managers.localization:text(carry.name_id), callback = givebag, data = i } )
			end
		end
	end
	table.insert( opts, { text = "Close", is_cancel_button = true } )
	mymenu = SimpleMenu:new("Super Loot Giver 69000", "Select the loot you want to secure.", opts)
	mymenu:show()
end

function doClassic()
	opts = {}
	table.insert( opts, { text = managers.localization:text("hud_carry_money"), callback = givebag, data = "money" } )
	table.insert( opts, { text = managers.localization:text("hud_carry_gold"), callback = givebag, data = "gold" } )
	table.insert( opts, { text = managers.localization:text("hud_carry_coke"), callback = givebag, data = "coke" } )
	table.insert( opts, { text = managers.localization:text("hud_carry_meth"), callback = givebag, data = "meth", is_focused_button = true } )
	table.insert( opts, { text = managers.localization:text("hud_carry_painting"), callback = givebag, data = "painting" } )
	table.insert( opts, { text = managers.localization:text("hud_carry_diamonds"), callback = givebag, data = "diamonds" } )
	table.insert( opts, { text = managers.localization:text("hud_carry_weapon"), callback = givebag, data = "weapon" } )
	table.insert( opts, { text = managers.localization:text("hud_carry_circuit"), callback = givebag, data = "circuit" } )
	table.insert( opts, { text = "Close", is_cancel_button = true } )
	mymenu = SimpleMenu:new("Super Loot Giver 69000", "Select the loot you want to secure.", opts)
	mymenu:show()
end

function isNonDLCItem(name)
	local list = {"hud_carry_money", "hud_carry_gold", "hud_carry_coke", "hud_carry_meth", "hud_carry_painting", "hud_carry_diamonds", "hud_carry_weapon", "hud_carry_circuit", "hud_carry_weapons"}
	for _,classic in pairs(list) do
		if (name == classic) then
			return true
		end
	end
	return false
end

function doDLC()
	opts = {}
	for i, carry in pairs(tweak_data.carry) do
		if ( carry ) then
			if carry.name_id and carry.bag_value and (isNonDLCItem(carry.name_id) == false) then
				if ((carry.bag_value == "cro_loot" and carry.type == "heavy") or (carry.bag_value == "mus_artifact_bag" and carry.unit ~= nil)) == false then
					table.insert( opts, { text = managers.localization:text(carry.name_id), callback = givebag, data = i } )
				end
			end
		end
	end
	table.insert( opts, { text = "Close", is_cancel_button = true } )
	mymenu = SimpleMenu:new("Super Loot Giver 69000", "Select the loot you want to secure.", opts)
	mymenu:show()
end

if ( managers.player:player_unit() ) then
	opts = {}
	table.insert( opts, { text = "Original PD2 Loot", callback = doClassic, is_focused_button = true } )
	table.insert( opts, { text = "DLC", callback = doDLC } )
	table.insert( opts, { text = "All", callback = doGeneric } )
	table.insert( opts, { text = "Close", is_cancel_button = true } )
	mymenu = SimpleMenu:new("Super Loot Giver 69000", "Select the loot you want to secure.", opts)
	mymenu:show()
end