if not _preplanningTweakDataInit then _preplanningTweakDataInit = PrePlanningTweakData.init end
function PrePlanningTweakData:init(...)
	_preplanningTweakDataInit(self, ...)
	self.gui.MAX_DRAW_POINTS = math.huge
end