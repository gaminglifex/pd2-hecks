local armor_init = tweak_data.player.damage.ARMOR_INIT
function PlayerCarry:_get_max_walk_speed(...)
	local multiplier = 1
	if managers.player:has_category_upgrade("player", "armor_carry_bonus") then
		local base_max_armor = armor_init + managers.player:body_armor_value("armor") + managers.player:body_armor_skill_addend()
		local mul = managers.player:upgrade_value("player", "armor_carry_bonus", 1)
		for i = 1, base_max_armor do
			multiplier = multiplier * mul
		end
		multiplier = math.clamp(multiplier, 0, 1)
	end
	return PlayerCarry.super._get_max_walk_speed(self, ...) * multiplier
end