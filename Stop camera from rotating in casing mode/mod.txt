{
	"name": "Stop camera from rotating in casing mode",
	"description": "Disable the super unrealistic bob when walking.",
	"author": "gir489",
	"contact": "https://bitbucket.org/gir489/",
	"version": "2",
	"priority": 1000,
	"hooks" : [
		{ "hook_id" : "lib/units/beings/player/states/playermaskoff", "script_path" : "playermaskoff.lua" },
		{ "hook_id" : "lib/units/beings/player/states/playerclean", "script_path" : "playerclean.lua" },
		{ "hook_id" : "lib/units/beings/player/states/playercivilian", "script_path" : "playercivilian.lua" }
	]
}