if not _sentActivate then _sentActivate = SentryGunFireModeInteractionExt.can_interact end
function SentryGunFireModeInteractionExt:can_interact(player)
	local key = LuaModManager:PlayerKeybinds()["DisableSentryInteractKey"]
	if ( string.is_nil_or_empty( key ) ) then
		return _sentActivate(self, player)
	end
	return _sentActivate(self, player) and Input:keyboard():down( Idstring(key) )
end