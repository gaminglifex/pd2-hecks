
--managers.money:_add_to_total(500000000) -- money

function giveitems( times, category, addToExistingItems )
	for id,item in pairs(tweak_data.blackmarket[category]) do
		local global_value = "normal"
		local skip = (item.inaccessible == true and category == "masks")
		if (item.unatainable == true) then
			skip = true
		end
		if (id == "plastic" or id == "no_color_no_material" or id == "no_color_full_material" or id == "nothing") then
			skip = true
		end
		if item.dlc then
			if ( managers.dlc:is_dlc_unlocked(item.dlc) ) then
				global_value = item.dlc
			else
				skip = true
			end
		elseif item.infamous then
			global_value = "infamous"
		elseif item.global_value then
			global_value = item.global_value
		elseif item.dlcs then
			for _, dlc in pairs( item.dlcs or {} ) do
				if ( managers.dlc:is_dlc_unlocked(item.dlc) ) then
					global_value = dlc
				else
					skip = true
				end
			end
		end
		if ( skip == false ) then
			if ( managers.blackmarket:has_item(global_value, category, id) == false ) then
				--log("!NEW! " .. category .. " global_value: " .. tostring(global_value) .. " ID: " .. id)
				for i=1, times do
					managers.blackmarket:add_to_inventory(global_value, category, id, false)
				end
			elseif ( addToExistingItems == true ) then
				local logged = false
				while managers.blackmarket._global.inventory[ global_value ][ category ][ id ] < times do
					if ( logged == false ) then
						--log("!OLD! " .. category .. " global_value: " .. tostring(global_value) .. " ID: " .. id)
						logged = true
					end
					managers.blackmarket:add_to_inventory(global_value, category, id, false)
				end
			end
		end
	end
	managers.blackmarket:_cleanup_blackmarket()
end


--[[giveitems(100, "materials", true)
giveitems(100, "textures", true)
giveitems(100, "colors", true)
giveitems(100, "masks", true)
giveitems(100, "weapon_mods", true)]]

if ( managers.player:player_unit() ) then

	--[[local missionId = nil
	for key,_ in pairs(managers.objectives:get_active_objectives()) do
		if ( missionId == nil ) then
			for numberMatch in key:gmatch("%d+") do 	
				missionId = tonumber(numberMatch)
			end
		end
	end
	log(tostring(missionId))]]
	
	--log(managers.job:current_level_id())

	--[[local wp_pos = Vector3()
	local wp_dir = Vector3()
	local wp_dir_normalized = Vector3()
	local wp_cam_forward = Vector3()
	local wp_onscreen_direction = Vector3()
	local wp_onscreen_target_pos = Vector3()
	function HUDManager:_update_waypoints(t, dt)
		local cam = managers.viewport:get_current_camera()
		if not cam then
			return
		end
		local cam_pos = managers.viewport:get_current_camera_position()
		local cam_rot = managers.viewport:get_current_camera_rotation()
		mrotation.y(cam_rot, wp_cam_forward)
		for id, data in pairs(self._hud.waypoints) do
			local panel = data.bitmap:parent()
			if data.state == "dirty" then
			end
			if data.state == "sneak_present" then
				data.current_position = Vector3(panel:center_x(), panel:center_y())
				data.bitmap:set_center_x(data.current_position.x)
				data.bitmap:set_center_y(data.current_position.y)
				data.slot = nil
				data.current_scale = 1
				data.state = "present_ended"
				data.text_alpha = 0.5
				data.in_timer = 0
				data.target_scale = 1
				if data.distance then
					data.distance:set_visible(true)
				end
			elseif data.state == "present" then
				data.current_position = Vector3(panel:center_x() + data.slot_x, panel:center_y() + panel:center_y() / 2)
				data.bitmap:set_center_x(data.current_position.x)
				data.bitmap:set_center_y(data.current_position.y)
				data.text:set_center_x(data.bitmap:center_x())
				data.text:set_top(data.bitmap:bottom())
				data.present_timer = data.present_timer - dt
				if 0 >= data.present_timer then
					data.slot = nil
					data.current_scale = 1
					data.state = "present_ended"
					data.text_alpha = 0.5
					data.in_timer = 0
					data.target_scale = 1
					if data.distance then
						data.distance:set_visible(true)
					end
				end
			else
				if data.text_alpha ~= 0 then
					data.text_alpha = math.clamp(data.text_alpha - dt, 0, 1)
					data.text:set_color(data.text:color():with_alpha(data.text_alpha))
				end
				data.position = data.unit and data.unit:position() or data.position
				mvector3.set(wp_pos, self._saferect:world_to_screen(cam, data.position))
				mvector3.set(wp_dir, data.position)
				mvector3.subtract(wp_dir, cam_pos)
				mvector3.set(wp_dir_normalized, wp_dir)
				mvector3.normalize(wp_dir_normalized)
				local dot = mvector3.dot(wp_cam_forward, wp_dir_normalized)
				if dot < 0 or panel:outside(mvector3.x(wp_pos), mvector3.y(wp_pos)) then
					if data.state ~= "offscreen" then
						data.state = "offscreen"
						data.arrow:set_visible(true)
						data.bitmap:set_color(data.bitmap:color():with_alpha(0.75))
						data.off_timer = 0 - (1 - data.in_timer)
						data.target_scale = 0.75
						if data.distance then
							data.distance:set_visible(false)
						end
						if data.timer_gui then
							data.timer_gui:set_visible(false)
						end
					end
					local direction = wp_onscreen_direction
					local panel_center_x, panel_center_y = panel:center()
					mvector3.set_static(direction, wp_pos.x - panel_center_x, wp_pos.y - panel_center_y, 0)
					mvector3.normalize(direction)
					local distance = data.radius * tweak_data.scale.hud_crosshair_offset_multiplier
					local target_pos = wp_onscreen_target_pos
					mvector3.set_static(target_pos, panel_center_x + mvector3.x(direction) * distance, panel_center_y + mvector3.y(direction) * distance, 0)
					data.off_timer = math.clamp(data.off_timer + dt / data.move_speed, 0, 1)
					if data.off_timer ~= 1 then
						mvector3.set(data.current_position, math.bezier({
							data.current_position,
							data.current_position,
							target_pos,
							target_pos
						}, data.off_timer))
						data.current_scale = math.bezier({
							data.current_scale,
							data.current_scale,
							data.target_scale,
							data.target_scale
						}, data.off_timer)
						data.bitmap:set_size(data.size.x * data.current_scale, data.size.y * data.current_scale)
					else
						mvector3.set(data.current_position, target_pos)
					end
					data.bitmap:set_center(mvector3.x(data.current_position), mvector3.y(data.current_position))
					data.arrow:set_center(mvector3.x(data.current_position) + direction.x * 24, mvector3.y(data.current_position) + direction.y * 24)
					local angle = math.X:angle(direction) * math.sign(direction.y)
					data.arrow:set_rotation(angle)
					if data.text_alpha ~= 0 then
						data.text:set_center_x(data.bitmap:center_x())
						data.text:set_top(data.bitmap:bottom())
					end
				else
					if data.state == "offscreen" then
						data.state = "onscreen"
						data.arrow:set_visible(false)
						data.bitmap:set_color(data.bitmap:color():with_alpha(1))
						data.in_timer = 0 - (1 - data.off_timer)
						data.target_scale = 1
						if data.distance then
							data.distance:set_visible(true)
						end
						if data.timer_gui then
							data.timer_gui:set_visible(true)
						end
					end
					local alpha = 0.8
					if dot > 0.99 then
						alpha = math.clamp((1 - dot) / 0.01, 0.4, alpha)
					end
					if data.bitmap:color().alpha ~= alpha then
						data.bitmap:set_color(data.bitmap:color():with_alpha(alpha))
						if data.distance then
							data.distance:set_color(data.distance:color():with_alpha(alpha))
						end
						if data.timer_gui then
							data.timer_gui:set_color(data.bitmap:color():with_alpha(alpha))
						end
					end
					if data.in_timer ~= 1 then
						data.in_timer = math.clamp(data.in_timer + dt / data.move_speed, 0, 1)
						mvector3.set(data.current_position, math.bezier({
							data.current_position,
							data.current_position,
							wp_pos,
							wp_pos
						}, data.in_timer))
						data.current_scale = math.bezier({
							data.current_scale,
							data.current_scale,
							data.target_scale,
							data.target_scale
						}, data.in_timer)
						data.bitmap:set_size(data.size.x * data.current_scale, data.size.y * data.current_scale)
					else
						mvector3.set(data.current_position, wp_pos)
					end
					data.bitmap:set_center(mvector3.x(data.current_position), mvector3.y(data.current_position))
					if data.text_alpha ~= 0 then
						data.text:set_center_x(data.bitmap:center_x())
						data.text:set_top(data.bitmap:bottom())
					end
					if data.distance then
						local length = wp_dir:length()
						data.distance:set_text(data._waypoint_id)
						data.distance:set_center_x(data.bitmap:center_x())
						data.distance:set_top(data.bitmap:bottom())
					end
				end
			end
			if data.timer_gui then
				data.timer_gui:set_center_x(data.bitmap:center_x())
				data.timer_gui:set_bottom(data.bitmap:top())
				if data.pause_timer == 0 then
					data.timer = data.timer - dt
					local text = 0 > data.timer and "00" or (math.round(data.timer) < 10 and "0" or "") .. math.round(data.timer)
					data.timer_gui:set_text(text)
				end
			end
		end
	end
	
	function add_waypoint(unit, key)
		local waypoint_id = tostring(key)
		managers.hud:add_waypoint( waypoint_id, { icon = 'scrollbar_arrow', distance = true, position = unit:position(), no_sync = true, present_timer = 0, state = "present", radius = 800, color = Color.white, blend_mode = "add" }  )
		local waypoint = managers.hud._hud.waypoints[waypoint_id]
		waypoint._waypoint_id = waypoint_id
		waypoint._unit = unit
	end
	
	local clearWaypoints = false
	
	if clearWaypoints then
		managers.hud:clear_waypoints()
	else
		local waypoint = managers.hud._hud.waypoints['3550']
		log(tostring(waypoint._unit:name()))
	end
	
	if clearWaypoints then
		for key, unit in ipairs( World:find_units_quick( "all" ) ) do
			local position = unit:position() - managers.player:player_unit():position()
			if ( position:length() < 500 and key > 100 ) then
				add_waypoint(unit, key)
			end
		end
	end]]
	
	--local mission_script = managers.mission:script("default")._elements[101280]
	--mission_script:on_executed(managers.player:player_unit())
	
	--Refill ammo
	for id,weapon in pairs( managers.player:player_unit():inventory()._available_selections ) do
		weapon.unit:base():add_ammo_from_bag( 34 )
		managers.hud:set_ammo_amount( id, weapon.unit:base():ammo_info() )
	end
	
	-- Carry mods (throwing distance, movement speed, jumping, running) 
	local car_arr = { 'being', 'mega_heavy', 'heavy', 'medium', 'light', 'coke_light', 'explosives' }
	for _,name in ipairs(car_arr) do
		if ( tweak_data.carry.types[ name ].throw_distance_multiplier < 1 ) then
			tweak_data.carry.types[ name ].throw_distance_multiplier = tweak_data.carry.types[ name ].throw_distance_multiplier + .2
		end
	end
	
	-- Allow infinite pagers (Only works when host.)
	function GroupAIStateBase:on_successful_alarm_pager_bluff()
		if ( self._nr_successful_alarm_pager_bluffs <= 2 ) then
			self._nr_successful_alarm_pager_bluffs = self._nr_successful_alarm_pager_bluffs + 1
		end
	end
	
	if not _initmBlocked then _initmBlocked = IntimitateInteractionExt._interact_blocked end 
	function IntimitateInteractionExt:_interact_blocked( player )
		if self.tweak_data == "corpse_dispose" then
			if managers.player:is_carrying() then
				return true
			end
			
			return false
		end
		return _initmBlocked(self, player)
	end
	
	--Highlight stuff for FFDay 3
	if managers.job:current_level_id() == "framing_frame_3" then
		for _,v in pairs(managers.mission._scripts.default._elements) do
			if v._values and v._values.trigger_list and v._values.trigger_list[1] and (v._values.trigger_list[1].notify_unit_sequence == "state_outline_enabled" or v._values.trigger_list[1].notify_unit_sequence == "enable_outline") then
				v:on_executed()
			end
		end
	end

	--Allow to use anything.
	if not _hasRequired then _hasRequired = BaseInteractionExt._has_required_deployable end 
	function BaseInteractionExt:_has_required_deployable()
		if Input:keyboard():down( Idstring("left alt") ) then
			return true
		else
			return _hasRequired(self)
		end
	end
	
	--Activate all C4 markers.
	if Input:keyboard():down( Idstring("left alt") ) then
		local unit_list = World:find_units_quick( "all" )
		for _,unit in ipairs( unit_list ) do
			if ( tostring(unit:name()) == "Idstring(@ID34d05ca941b5ebfe@)" --[[C4 Placement Marker]] ) then
				if ( unit:interaction():active() == true ) then
					unit:interaction():interact(managers.player:player_unit())
				end
			end
		end
	end
	
	--Check for items to pick up.
	if Input:keyboard():down( Idstring("right shift") ) then
		local unit_list = World:find_units_quick( "all" )
		for _,unit in ipairs( unit_list ) do
			if ( tostring(unit:name()) == "Idstring(@ID5422d8b99c7c1b57@)" --[[Security Card]] ) then
				if ( unit:interaction():active() == true ) then
					if not managers.player:has_special_equipment("bank_manager_key") then
						unit:interaction():interact(managers.player:player_unit())
						break
					end
				end
			elseif ( (tostring(unit:name()) == "Idstring(@ID859ce3831a8671f4@)" ) --[[Crowbarb]] ) then
				if ( unit:interaction():active() == true ) then
					if not managers.player:has_special_equipment("crowbar") then
						unit:interaction():interact(managers.player:player_unit())
						break
					end
				end
			end
		end
	end
	
	--For logging Idstring
	function BaseInteractionExt:interact( player )
		self:_post_event( player, "sound_done" )
		log(self._unit:interaction().tweak_data .. " " .. tostring(self._unit:name()))
	end
	
	function GatoraidMeBitch(name)
		local special_equip = managers.player._equipment.specials[name]
		if special_equip == nil then
			return true
		end
		local deigested_amount = special_equip.amount and Application:digest_value(special_equip.amount, false) or 1
		return deigested_amount < 1
	end
	
	--Look for and pickup any chemicles.
	if Input:keyboard():down( Idstring("right shift") ) then
		if managers.job:current_level_id() == "mia_1" or managers.job:current_level_id() == "alex_1" or managers.job:current_level_id() == "rat" then
			for _,unit in ipairs( World:find_units_quick( "all" )  ) do
				if ( tostring(unit:name()) == "Idstring(@IDf1461ef41b1bce71@)" ) then --[[MU]]
					if ( unit:interaction():active() ) then
						--io.write("Found MU\n")
						if ( GatoraidMeBitch("acid")) then
							unit:interaction():interact(managers.player:player_unit())
							break
						end
					end
				end
				if ( tostring(unit:name()) == "Idstring(@ID7dc5f755ea3f7d65@)" ) then --[[CS]]
					if ( unit:interaction():active() ) then
						--io.write("Found CS\n")
						if ( GatoraidMeBitch("caustic_soda")) then
							unit:interaction():interact(managers.player:player_unit())
							break
						end
					end
				end
				if ( tostring(unit:name()) == "Idstring(@ID4e2b16fa53a8d1f3@)" ) then --[[HCL]]
					if ( unit:interaction():active() ) then
						--io.write("Found HCL\n")
						if ( GatoraidMeBitch("hydrogen_chloride")) then
							unit:interaction():interact(managers.player:player_unit())
							break
						end
					end
				end
			end
		end
	end
	
	--Holding RCTRL and running the script will cause you to pick yourself up if downed.
	if Input:keyboard():down( Idstring("right ctrl") ) then
		managers.player:player_unit():character_damage():revive(managers.player:player_unit())
	end
	
	--Find Gage packages and pick them up if holding right alt.
	if Input:keyboard():down( Idstring("right alt") ) then
		local found = false
		local unit_list = World:find_units_quick( "all" ) 
		for _,unit in ipairs( unit_list ) do 
			if ( tostring(unit:name()) == "Idstring(@IDe8088e3bdae0ab9e@)" --[[Yellow Package]] or
				 tostring(unit:name()) == "Idstring(@ID05956ff396f3c58e@)" --[[Blue Package]] or
				 tostring(unit:name()) == "Idstring(@IDc90378ad89058c7d@)" --[[Purple Package]] or
				 tostring(unit:name()) == "Idstring(@ID96504ebd40f8cf98@)" --[[Red Package]] or
				 tostring(unit:name()) == "Idstring(@IDb3cc2abe1734636c@)" --[[Green Package]] ) then
				if ( found == false ) then
					unit:interaction():interact(managers.player:player_unit()) 
					found = true
				end
				unit:on_executed()
			end
		end
	end
	
	--[[local pivot_shoulder_translation = Vector3( 10.64, 23, -5.1 )
	local pivot_shoulder_rotation = Rotation( 0, 0, 0 )
	local pivot_head_rotation = Rotation( 0, 0, -0.5 )
	tweak_data.player.stances.rpk.steelsight.shoulders.translation = Vector3( 0,0,0 ) - pivot_shoulder_translation:rotate_with( pivot_shoulder_rotation:inverse() ):rotate_with( pivot_head_rotation )
	tweak_data.player.stances.rpk.steelsight.shoulders.rotation = pivot_head_rotation
	
	tweak_data.weapon.factory.parts.wpn_fps_upg_o_acog.stance_mod["wpn_fps_lmg_rpk"] = { translation = Vector3(0.01, 1, -3.065), rotation = Rotation(-0.05, 0, 0) }
	tweak_data.weapon.factory.parts.wpn_fps_upg_o_specter.stance_mod["wpn_fps_lmg_rpk"] = { translation = Vector3(0, -2, -3.065), rotation = Rotation(-0.05, 0, 0) }]]
	
	--DooM 2 Shotgun
	tweak_data.player.stances[ "huntsman" ].standard.shoulders.translation = tweak_data.player.stances[ "huntsman" ].steelsight.shoulders.translation
	tweak_data.player.stances[ "huntsman" ].standard.shoulders.rotation = tweak_data.player.stances[ "huntsman" ].steelsight.shoulders.rotation
	tweak_data.player.stances[ "huntsman" ].crouched.shoulders.translation = tweak_data.player.stances[ "huntsman" ].steelsight.shoulders.translation
	tweak_data.player.stances[ "huntsman" ].crouched.shoulders.rotation = tweak_data.player.stances[ "huntsman" ].steelsight.shoulders.rotation
		
	--Disable respawning guards.
	if Network:is_server() then
		for _, script in pairs(managers.mission:scripts()) do
			for id, element in pairs(script:elements()) do
				local name = element:editor_name()
				if name == "spawnCopPatrol" or name == "spawn_guard" or name == "respawn_guard" or name:find("extra_spawn") then
					element:set_enabled(false)
				end
			end
		end
	end
end