--[[ * SimpleMenu implementation of Change Equipment by gir489 v6
	 *
	 * Credits; 
	 *			Harfatus for SimpleMenu.
	 *
	 * Changelog: 
	 *		v1: Initial release
	 *		v2: Split melee weapons in to two groups.
	 *		v3: Fixed skins not working.
	 *			Made the default cursor position your currently equiped weapon.
	 *		v4: Added check to see if the user owns the DLC that the melee weapon belongs to.
	 *		v5: Made changing primary/secondary not crash the game.
	 *		v6: Added change mask.
	 *
	 *	Not for use in Pirate Perfagtion Trainer]]

if not SimpleMenu then
    SimpleMenu = class()

    function SimpleMenu:init(title, message, options)
        self.dialog_data = { title = title, text = message, button_list = {},
                             id = tostring(math.random(0,0xFFFFFFFF)) }
        self.visible = false
        for _,opt in ipairs(options) do
            local elem = {}
            elem.text = opt.text
            opt.data = opt.data or nil
            opt.callback = opt.callback or nil
            elem.callback_func = callback(self, self, "_do_callback",
                                          { data = opt.data,
                                            callback = opt.callback})
            elem.cancel_button = opt.is_cancel_button or false
            if opt.is_focused_button then
                self.dialog_data.focus_button = #self.dialog_data.button_list+1
            end
            table.insert(self.dialog_data.button_list, elem)
        end
        return self
    end

    function SimpleMenu:_do_callback(info)
        if info.callback then
            if info.data then
                info.callback(info.data)
            else
                info.callback()
            end
        end
        self.visible = false
    end

    function SimpleMenu:show()
        if self.visible then
            return
        end
        self.visible = true
        managers.system_menu:show(self.dialog_data)
    end

    function SimpleMenu:hide()
        if self.visible then
            managers.system_menu:close(self.dialog_data.id)
            self.visible = false
            return
        end
    end
end

patched_update_input = patched_update_input or function (self, t, dt )
    if self._data.no_buttons then
        return
    end
    
    local dir, move_time
    local move = self._controller:get_input_axis( "menu_move" )

    if( self._controller:get_input_bool( "menu_down" )) then
        dir = 1
    elseif( self._controller:get_input_bool( "menu_up" )) then
        dir = -1
    end
    
    if dir == nil then
        if move.y > self.MOVE_AXIS_LIMIT then
            dir = 1
        elseif move.y < -self.MOVE_AXIS_LIMIT then
            dir = -1
        end
    end

    if dir ~= nil then
        if( ( self._move_button_dir == dir ) and self._move_button_time and ( t < self._move_button_time + self.MOVE_AXIS_DELAY ) ) then
            move_time = self._move_button_time or t
        else
            self._panel_script:change_focus_button( dir )
            move_time = t
        end
    end

    self._move_button_dir = dir
    self._move_button_time = move_time
    
    local scroll = self._controller:get_input_axis( "menu_scroll" )
    -- local sdir
    if( scroll.y > self.MOVE_AXIS_LIMIT ) then
        self._panel_script:scroll_up()
        -- sdir = 1
    elseif( scroll.y < -self.MOVE_AXIS_LIMIT ) then
        self._panel_script:scroll_down()
        -- sdir = -1
    end
end
managers.system_menu.DIALOG_CLASS.update_input = patched_update_input
managers.system_menu.GENERIC_DIALOG_CLASS.update_input = patched_update_input

function spawnprimarycallback(info)
	if ( managers.player:player_unit() ) then
		local weapon = Global.blackmarket_manager.crafted_items.primaries[info]
		if weapon then
			equipWeapon("primaries", info)
		end
	end
end

function spawnsecondarycallback(info)
	if ( managers.player:player_unit() ) then
		local weapon = Global.blackmarket_manager.crafted_items.secondaries[info]
		if weapon then
			equipWeapon("secondaries", info)
		end
	end
end

function equiparmorcallback(info)
	if ( managers.player:player_unit() ) then
		if info then
			managers.blackmarket:equip_armor(info)
			managers.network:session():send_to_peers_synched("set_unit", managers.player:player_unit(), managers.network:session():local_peer():character(), managers.blackmarket:outfit_string(), managers.network:session():local_peer():outfit_version(), managers.network:session():local_peer():id())
			managers.player:player_unit():_reload_outfit()
		end
	end
end

--Doesn't work as client. Requires switching to a primary weapon that has a skin to show as client. Not going to fix it because purely cosmetic.
function changemaskcallback(info)
	if ( managers.player:player_unit() ) then
		if info then
			managers.player:set_player_state("ingame_mask_off")
			managers.player:player_unit():movement():current_state()._state_data.mask_equipped = false
			managers.player:local_player():camera():camera_unit():base():unspawn_mask()
			managers.player:player_unit():inventory():set_mask_visibility(false)
			managers.blackmarket:equip_mask(info)
			managers.network:session():send_to_peers_synched("set_unit", managers.player:player_unit(), managers.network:session():local_peer():character(), managers.blackmarket:outfit_string(), managers.network:session():local_peer():outfit_version(), managers.network:session():local_peer():id())
			managers.player:player_unit():inventory():set_mask_visibility(true)
			local equipped_mask = managers.blackmarket:equipped_mask()
			local peer_id = managers.network:session():local_peer():id()
			local mask_id = equipped_mask.mask_id and managers.blackmarket:get_real_mask_id(equipped_mask.mask_id, peer_id)
			local mask_unit_name = managers.blackmarket:mask_unit_name_by_mask_id(mask_id, peer_id)
			if not managers.dyn_resource:is_resource_ready(Idstring("unit"), Idstring(mask_unit_name), managers.dyn_resource.DYN_RESOURCES_PACKAGE) then
				managers.dyn_resource:load(Idstring("unit"), Idstring(mask_unit_name), managers.dyn_resource.DYN_RESOURCES_PACKAGE, nil)
			end
			managers.player:set_player_state("standard")
		end
	end
end

function spawnmeleecallback(info)
	if ( managers.player:player_unit() ) then
		if info then
			managers.blackmarket:equip_melee_weapon(info)
			managers.network:session():send_to_peers_synched("set_unit", managers.player:player_unit(), managers.network:session():local_peer():character(), managers.blackmarket:outfit_string(), managers.network:session():local_peer():outfit_version(), managers.network:session():local_peer():id())
			managers.player:player_unit():_reload_outfit()
		end
	end
end

function doPrimary(info)
	opts = {}
	for i,weapon in pairs(Global.blackmarket_manager.crafted_items.primaries) do
		if ( weapon ) then
			table.insert( opts, { text = managers.blackmarket:get_weapon_name_by_category_slot("primaries",i):gsub("\"", ""), callback = spawnprimarycallback, data = i, is_focused_button = weapon.equipped } )
		end
	end
	table.insert( opts, { text = "Close", is_cancel_button = true } )
	mymenu = SimpleMenu:new("Change Primary", "Select the primary weapon you want.", opts)
	mymenu:show()
end

function doArmor(info)
	opts = {}
	for i,armor in pairs(Global.blackmarket_manager.armors) do
		if ( armor ) then
			table.insert( opts, { text = managers.localization:text(tweak_data.blackmarket.armors[i].name_id), callback = equiparmorcallback, data = i, is_focused_button = armor.equipped } )
		end
	end
	table.insert( opts, { text = "Close", is_cancel_button = true } )
	mymenu = SimpleMenu:new("Change Armor", "Select the armor you want.", opts)
	mymenu:show()
end

function doSecondary(info)
	opts = {}
	for i,weapon in pairs(Global.blackmarket_manager.crafted_items.secondaries) do
		if ( weapon ) then
			table.insert( opts, { text = managers.blackmarket:get_weapon_name_by_category_slot("secondaries",i):gsub("\"", ""), callback = spawnsecondarycallback, data = i, is_focused_button = weapon.equipped } )
		end
	end
	table.insert( opts, { text = "Close", is_cancel_button = true } )
	mymenu = SimpleMenu:new("Change Secondary", "Select the secondary weapon you want.", opts)
	mymenu:show()
end

function doMeleeSharp(info)
	opts = {}
	for i,melee in pairs(Global.blackmarket_manager.melee_weapons) do
		if ( melee ) then
			local melee_tweak = tweak_data.blackmarket.melee_weapons[i]
			if ( melee_tweak.stats.weapon_type == "sharp" and managers.dlc:has_dlc( melee_tweak.dlc ) ) then
				table.insert( opts, { text = managers.localization:text(melee_tweak.name_id), callback = spawnmeleecallback, data = i, is_focused_button = melee.equipped } )
			end
		end
	end
	table.insert( opts, { text = "Close", is_cancel_button = true } )
	mymenu = SimpleMenu:new("Change Melee(Sharp)", "Select the melee weapon you want.", opts)
	mymenu:show()
end

function doMeleeBlunt(info)
	opts = {}
	for i,melee in pairs(Global.blackmarket_manager.melee_weapons) do
		if ( melee ) then
			local melee_tweak = tweak_data.blackmarket.melee_weapons[i]
			if ( melee_tweak.stats.weapon_type == "blunt" and managers.dlc:has_dlc( melee_tweak.dlc ) ) then
				table.insert( opts, { text = managers.localization:text(melee_tweak.name_id), callback = spawnmeleecallback, data = i, is_focused_button = melee.equipped } )
			end
		end
	end
	table.insert( opts, { text = "Close", is_cancel_button = true } )
	mymenu = SimpleMenu:new("Change Melee(Blunt)", "Select the melee weapon you want.", opts)
	mymenu:show()
end

function doMask(info)
	opts = {}
	for i,mask in pairs(Global.blackmarket_manager.crafted_items.masks) do
		if ( mask ) then
			table.insert( opts, { text = managers.blackmarket:get_mask_name_by_category_slot("masks",i):gsub("\"", ""), callback = changemaskcallback, data = i } )
		end
	end
	table.insert( opts, { text = "Close", is_cancel_button = true } )
	mymenu = SimpleMenu:new("Change Mask", "Select the mask you want.", opts)
	mymenu:show()
end

function NewRaycastWeaponBase:set_timer(timer, ...)
	NewRaycastWeaponBase.super.set_timer(self, timer)
	for _, data in pairs(self._parts) do
		if ( data.unit ) then
			data.unit:set_timer(timer)
			data.unit:set_animation_timer(timer)
		end
	end
end

function equipWeapon(category, slot)
	if not slot or slot > #managers.blackmarket._global.crafted_items[category] then
		Application:error("[BlackMarketManager:equip_weapon_in_game] invalid slot", slot)
		return
	end
	local primary = category == "primaries"
	local first_time = true
	local function clbk()
		if first_time then
			managers.blackmarket:equip_weapon(category, slot)
			first_time = false
		end
		if not managers.network:session():local_peer():is_outfit_loaded() then
			return false
		end
		local weapon = managers.blackmarket._global.crafted_items[category][slot]
		local texture_switches = managers.blackmarket:get_weapon_texture_switches(category, slot, weapon)
		managers.player:player_unit():inventory():add_unit_by_factory_name(weapon.factory_id, true, false, weapon.blueprint, weapon.cosmetics, texture_switches)
		return true
	end
	local factory_weapon = tweak_data.weapon.factory[managers.blackmarket._global.crafted_items[category][slot].factory_id]
	local ids_unit_name = Idstring(factory_weapon.unit)
	if not managers.dyn_resource:is_resource_ready(Idstring("unit"), ids_unit_name, managers.dyn_resource.DYN_RESOURCES_PACKAGE) then
		managers.dyn_resource:load(Idstring("unit"), ids_unit_name, managers.dyn_resource.DYN_RESOURCES_PACKAGE, nil)
	end
	managers.player:player_unit():movement():current_state():_start_action_unequip_weapon(TimerManager:game():time(), {
		selection_wanted = primary and 2 or 1,
		unequip_callback = clbk
	})
end

opts = {}
table.insert( opts, { text = "Change Primary", callback = doPrimary, data = nil } )
table.insert( opts, { text = "Change Secondary", callback = doSecondary, data = nil } )
table.insert( opts, { text = "Change Armor", callback = doArmor, data = nil } )
table.insert( opts, { text = "Change Melee(Sharp)", callback = doMeleeSharp, data = nil } )
table.insert( opts, { text = "Change Melee(Blunt)", callback = doMeleeBlunt, data = nil } )
table.insert( opts, { text = "Change Mask", callback = doMask, data = nil } )
table.insert( opts, { text = "AND GOOOOOOD RIDDANCE!", is_cancel_button = true } )
mymenu = SimpleMenu:new("YEP?", "WHATCHU WANT?!", opts)
mymenu:show()