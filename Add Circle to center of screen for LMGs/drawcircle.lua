function isLMG()
	if ( managers.player:player_unit() == nil ) then
		return false
	end
	if ( managers.player:player_unit():inventory():equipped_unit() == nil ) then
		return false
	end
	local equipped_weapon = managers.player:player_unit():inventory():equipped_unit():base():weapon_tweak_data()
	
	if equipped_weapon then
		if equipped_weapon.category == "lmg" or equipped_weapon.category == "minigun" then 
			return true
		end
	end
    return false
end

if ( isLMG() ) then
	if ( managers.player:local_player():movement():current_state()._state_data.in_steelsight ~= true ) then
		managers.hud:on_crit_confirmed()
	end
end