{
	"name": "Add Circle to center of screen for LMGs",
	"description": "If you're seeing a yellow X instead of a red circle, add the preU39CritConfirmed to your assets/mod_overrides folder included in this mod's folder.",
	"author": "gir489",
	"contact": "https://bitbucket.org/gir489/",
	"version": "1",
	"priority": 500,
	"persist_scripts" : [
		{ "global" : "DrawCricleForLMGs", "script_path" : "drawcircle.lua" }
	]
}