{
    "name" : "Force fuill reload",
    "description" : "Forces the game to use the full reload animation every time.",
    "author" : "gir489"
    "contact": "https://bitbucket.org/gir489/",
    "version" : "1",
    "priority" : 500,
	"hooks" : [
        {"hook_id" : "lib/units/weapons/newraycastweaponbase", "script_path" : "newraycastweaponbase.lua"},
        {"hook_id" : "lib/units/beings/player/states/playerstandard", "script_path" : "playerstandard.lua"}
	]
}