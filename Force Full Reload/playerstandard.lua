if not _startActionReload then _startActionReload = PlayerStandard._start_action_reload end
function PlayerStandard:_start_action_reload( t )
	if ( self._equipped_unit:base():weapon_tweak_data().timers.reload_empty ~= nil ) then
		local original = self._equipped_unit:base():get_ammo_remaining_in_clip()
		self._equipped_unit:base():set_ammo_remaining_in_clip(0)
		_startActionReload(self, t)
		self._equipped_unit:base():set_ammo_remaining_in_clip(original)
	else
		_startActionReload(self, t)
	end
end