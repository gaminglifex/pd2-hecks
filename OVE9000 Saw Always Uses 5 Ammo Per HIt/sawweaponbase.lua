if not _fireSaw then _fireSaw = SawWeaponBase.fire end
function SawWeaponBase:fire( from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul, target_unit )
	local ammo_use = self:get_ammo_remaining_in_clip()
	local ammo_total = self:get_ammo_total()
	
	local result = _fireSaw( self, from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul, target_unit ) 
	
	if managers.player:player_unit() == self._setup.user_unit then
		if ammo_total ~= self:get_ammo_total() then
			if ammo_total > 0 then
				self:set_ammo_remaining_in_clip( math.max( ammo_use - 5, 0 ) )
				self:set_ammo_total( math.max( ammo_total - 5, 0 ) )
			end
		end
	end
	
	return result
end